---
title: "DMStack"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Alligator Company (ACS) Modern Data Platform

{{< figure src="/dmstack.png" alt="ACS Modern Data Stack">}}

Created by: [Alligator Company GmbH](https://alligator-company.org)

- [ACS Workbench](https://gitlab.com/alligatorcompany/acs-operation)
- [ACS Documentation](https://gitlab.com/alligatorcompany/acs-dmstack-doc)
- [ACS DBT dwhgen](https://gitlab.com/alligatorcompany/dbt-dwhgen)
- [ACS DBT exasol](https://gitlab.com/alligatorcompany/dbt-exasol)
- [ACS DBT exasol-utils](https://gitlab.com/alligatorcompany/exasol-utils)

Partnered by: 
- [DBTLabs](https://www.getdbt.com/)
- [Exasol](https://www.exasol.com/)
- [Snowflake](https://www.snowflake.com/)
- [Datavault Builder](https://datavault-builder.com/)

Other Tools:
- [Postgres Citus](https://www.citusdata.com/)
- [DBTVault](https://dbtvault.readthedocs.io/en/latest/)
- [Superset](https://superset.apache.org/)
- [Argo Workflow](https://argoproj.github.io/)
- [K3D](https://k3d.io)
- [Kustomize](https://kustomize.io/)

## Scoping
This documentation contains guidelines, principles and approaches that are used in the ACS Data Platform. To keep the content concise and digestible, we decided to keep the following general topics as out of scope. Where appropriate, we add link to corresponding standard documentation, in order to make further reading easily accessible for the reader.
Out of Scope are:
- Standard Kubernetes Installation, Standard Resouce Definitions and kubectl commands
- Docker installation and cli usage
- Argo workflows general usage
- DBT basic usage
- Datavault modeling
- GIT versioning
- command line usage

In a way, basic, minimal knowledge should be available in order to understand this documentation and start with the ACS data platform, but using our templates, good practices and the ACS workbench with dbt-dwhgen and the disposable environments, first results should be achievable very fast.

## Information Model
The information model describes information elements and the requirements that user have when analyzing data in regard this information. This becomes more important with the amount of information we have available in times of digitalization. These information need to be formulated and categorized in order to be able to related to these information during communication and implementation. Without clear definitions and common language about topologies used in the business processes, the data that is being used for and produced by analytics is ambigous. Thus is produce unreliable results.

With ever growing challenges coming from sheer variety of information the need for speed in data integration and automation in those technical processes is also omnipresent. But we cannot give up quality for speed. We need to intertwine both. 
This is one reason to use flexible and agile methods like for business data modeling like ELM (ensemble logical modeling), which directly maps the information elements on to the data elements of an automated Datavault implementation.

The information model describes information elements and the requirements that user have when analyzing data in regard this information. This becomes more important with the amount of information we have available in times of digitalization. These information need to be formulated and categorized in order to be able to related to these information during communication and implementation. Without clear definitions and common language about topologies used in the business processes, the data that is being used for and produced by analytics is ambigous. Thus is produce unreliable results.

With ever growing challenges coming from sheer variety of information the need for speed in data integration and automation in those technical processes is also omnipresent. But we cannot give up quality for speed. We need to intertwine both. 
This is one reason to use flexible and agile methods like for business data modeling like ELM (ensemble logical modeling), which directly maps the information elements on to the data elements of an automated Datavault implementation.

The purpose of the information model is to describe the various information elements in terms of subject matter and semantics. As a basis, it includes the definition and structuring of the information elements, including their relationships to each other, and, based on this, their use by users or within processes.

The challenges today have increased enormously due to the growing amount of data and stricter regulations.
The level of interconnection is increasing. Here, the approaches from the field of knowledge management & semantic web (topic map, RDF) and the application of property graphs have proven their qualities.
These combine textual description and visualisation to make the inherent complexity of a networked world more easily accessible.


## Data Architecture and Management
The data architecture and management is based on the Datavault architecture and tries to consequently implement the 4QM (quadrant model) by Ronald Damhof in order to make this also transparent throughout the organization. 

``` 
Today, in an average organization the car park or art collection
is better managed then data
```

The data architecture tries to make sure your data is better organized by following the governance rules that can be derived from the data management quadrant.
## Implementation
The implementation uses state of the art Ingestion to source data, Datavault automation for integration and long-term storage and the Analytics Engineering approach with DBT in order to maintain and transformations on modern data platforms like Exasol, Snowflake, BigQuery and Redshift.
The execution and deployment in modern containerized platforms like Kubernetes are future-proof and follow cloud-native principles. Horizontal scaling for your data processes and the data teams delivering insights from your data.
## Analytics Engineering

Lastly, the Analytics Engineering guidelines and recipes make execution of development and maintenance standard with DataOps principles that ensure Software Engineering practices will also be followed in your data pipelines. This means industry standards that are driven by the community behind DBT.
