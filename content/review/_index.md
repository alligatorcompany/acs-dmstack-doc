---
title: "05 Review Guides"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Review Guides
Following our approach proposes the following general process:

1. ELM workshops for requirements - Backbone Model and ELM artifacts
2. Results documented in DBT project for the scoped product
3. Attribution will be change in Softrules

Therefor the review should always be started with the generated DBT documentation for the product in place. By doing so you set the focus on the most important delivery - which ist not the transformations, data or report - it is the common understanding and transparency between all participants - especially the business.

This is how the overview of a DBT project should look like:
```
1. Report Requirements
    * Visualization or Prototype: Screenshot with comments
    * Questions: Open question list
2. Data Requirements (ELM)
    * Enterprise Data Story: short business description of what information is to be shown or delivered
    * ELM Model: Screenshot of the ELM to be implemented and the CBC list with CBCs and descriptions
    * Dimensional Model: Fact Table grain and their dimensions - Screenshot of PostIt model
    * links to the ELM artifacts from the workshop
3. Functional Dataflow
    * Interfaces: List of sources, the source interfaces and their technical keys (might be different from business key), that need to be imported and special hints how to access them
    * Golden Source Rules: In case of multiple sources being integrated we need
    * Hardrules: special Hardrules if necessary (Datatypes are being conformed automatically). Remember those should be exceptions and not change any datasets contents
    * Mapping and Softrules: any rules that need to be applied before delivering the mart schema or report
```

## When and How a Review Should Take Place

The responsibility for the mandatory review meeting is on the user responsible for the User Story, that wants to introduce changes. Since these changes will only go into production once this quality gate has been passed, it should be in the interest of that user to get ahead with his/her User Story.

Preparations:
1. Create the Merge/Pull request in the git hosting service. Make sure the commit history can be explained in the scope of the User Story changes and the CI pipeline run is fresh and good
2. Prepare a short introduction to give scope to the participants, including scope of the product and scope of the introduced changes
3. Have the DBT documentation ready for inspection and re-assure all guidelines an be met
4. Schedule the meeting - participants should be a co-developers and optionally a business user if open issues occurred

During the meeting make sure the meeting is kept short and focused. This should not take more than a few minutes to half an hour. Make sure to create tasks in your ticketing system and/or document the OPL in the DBt project for all upcoming issues that need to be addressed.

## Review Checklist
This is a list of most important guidelines, that should be a starting list for your organization specific review guide.

Make sure to use the change history from your Merge/Pull request to scope your checks!

Generally - Hardrules, Datavault and Softrules all result in Data models. Thanks to automation, we do not have to manually create all artifacts anymore. Therefore we can stop worrying on big parts of the physical model checks. But in all cases these general guidelines should still be checked on all data models.

### Requirements level
- Check on ELM Model and overview existence
- Check wether all CBCs have proper subject area assigned
- Is the CBC business key really a business key? If system generated - why?
- Does the model answer all questions?
- Are there alternate, unique identifiers? Are they in a separate Bag of Keys (BOK) satellite? [Satellite Design Best Practices](https://www.youtube.com/watch?v=1cH76asmYTs)
- Are there PII, that need to be in separate Satellites?
- Is there a SBE satellite for business effectivity?
- Are there derived information Satellites and have they been defined properly?
- Satellite design - do we need refactoring for smaller number of satellites?

### Project level
- dbt_project.yml - check project name (Hardrule source name, Softrule product or report name)
- Do the schema suffixes conform to your naming standards
- Soft- and Hardrules should always be views globally
- Marts by default should be tables
- check on hooks (post, run-end, run-start) do they conform and are the right grants set
- Check dbt run and dbt test outputs on CI-pipeline run

### Package Management
- Are dependencies up to date?
- Are all local imports legal if it comes to the data architecture - Hardrules imports are not allowed

### Code Style
- sqlfluff and YAML linters in VSCode should be checked
- Exceptions need to be documented or proposed as standard changes

### Model Structure
- Are stages, rules and mart models well organized in folders
- Check [model lineage](https://docs.getdbt.com/docs/building-a-dbt-project/documentation#navigating-the-documentation-site) and check structure stg, rules, mart
- Check all model names for naming standards
- Is the code modular - one transformation per model?
- Is there one transformation per CTE and are CTEs used according to our style guide?
- Incremental models: Check key and is_incremental() macro existence
- In Softrules, filtering should be the earliest possible
- In Hardrules, no data changes may occur, are Hardrules really source-driven?
- Avoidance of macros - explain usage of macros?

### Documentation & Attribution
- Check documentation on existence (model and column level) and make sure you understand all descriptions
- Check attribute names and their typing in suffixes
- Is the documentation in sync with the scope of User Story / or changed artifacts?
- Check complex transformations  - they should have extra explanation in the overview.

### Datavault
- The ELM Backbone and the implemented version in the automation software should be mostly identical - exceptions should be explained or obvious
- Check implemented names (physical) whether they match to logical names from workshops
- Backbone model changes should be unique and checked to similar information that could be re-used. Softrules could replace duplicated CBC
- NBR - check granularity of the relationships. Do they introduce sparsity or duplication?

### Dimensional Model
- Does the grain of the fact table match the requirements?
- Are there not-null checks on all Foreign Keys of the fact table?
- Are there foreign-key checks on Dimensions in the fact table?
- Are there un-used Dimensions in the fact table? (check dbt lineage)
- Are there not-null checks on all PKs in Facts and Dimensions?

# Further Reading
- [Checklist for DBT project](https://discourse.getdbt.com/t/your-essential-dbt-project-checklist/1377)
- [DBT Guidance](https://blog.getdbt.com/one-analysts-guide-for-going-from-good-to-great/)
- [SQL Tutorial](https://mode.com/sql-tutorial/)
