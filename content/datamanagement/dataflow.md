---
title: "Dataflow"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Dataflow

In the dataflow overview it is shown 

{{< figure src="/flow.png" title="Dataflow" alt="Dataflow">}}

Each step is executed in its own docker image

1. Ingestion
acs-ingest docker image, that contains access clients for the source systems and the python code to execute the ingestion.

2. PSA Import and temporal
PSA import / temporal is a DBT project with an incremental model.

3. Hardrules Views
DBT project with view configuration.

4. DV Automation
Datavault Builder
DBTVault

5. Softrule Views (Business Vault)

6. Star Transformation - mart definition for BI applications

## Dataflow Execution

To execute this dataflow we configure the argo workflow engine with corresponding templates. 
Argo workflows are Kubernetes native way of executing Jobs. It consists of running components in the cluster, that introduce so called custom resource definitions (CRD). Those CRD allow you to define workflows as YAML definitions like any other pod. Therefor you Argo implementation is in the Repository in wf/... as Kind: Workflow or CronWorkflow and wf/template as base, re-usable templates.
In the repository you can find those in the wf folder:
```
├ dwh
...
└── 00_src
└── 01_dv
└── 02_app
└── 04_sbx
└── wf
    └── template
```

Corresponding templates are used to execute the different steps. 

### Ingesting source data
The ingestion depends most on the client environment and wether infrastructure runs on-prem, in-cloud or hybrid. Existing implementations include
- Upload through parquet files in S3 to use direct relational access via external tables or import function
- API access for Salesforce
- ODATA rest access
- relational transport through sqlalchemy

In all cases ingestion is used to ultimately fill json into our PSA_INSERT table structure. This is the generalized structure that uses JSON payload for the records of imported sources.

The PSA builds delta increments from the source imports - regardless of whether the source ingest is full or incremental.
### Datavault Builder deployment
The Datavault Builder deployment currently relies on the shell script exported by the vendor. The [rollout.sh](https://datavault-builder.com/wp-content/uploads/documentation-portal/latest_6/_modules/deployment.html?highlight=rollout#direct-deployment) called file can be used as full deployment and delta differences to deploy on to of the current target environment.

### Datavault Builder job execution
The job execution is based on the REST API provided by the vendor. The job execution can be controlled by a simple text file containing the job names that need to be executed.
This way the pipeline can differentiate which jobs to execute for Hardrules and which ones based on Softrules.
### DBT execution - elt-run
The DBT run execution is based on the dbt CLI, of course. The different projects need to be executed in correct order
1. PSA dbt project
2. all Hardrule projects
3. Common projects
4. Concrete application projects

In addition to the order of execution it needs to be taken care of the profiles used. By default we use a profiles with environment variables set, in order to keep the profiles simple. Therefore we copy a profiles.yml in to every project and export the DBT_PROFILES_DIR=. environment variable.
