---
title: "Data Architecture"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Data Architecture

The data architecture is inspired by the data quadrant and the datavault architecture embracing the separation of concerns and information hiding principles from industry standards.

The implementation of the data architecture is adjusted towards Analytics Engineering principles and modern relational data platforms. Consequently the complete processing is executed as ELT within the relational database engine.
In the overview we depict the purpose, use of models and processes as follows:
{{< figure src="/da_overview.png" title="Data Architecture Layers" alt="Data Architecture Layers">}}

Compared to the Quadrant model the Data architecture introduces additional layers for the PSA and the Hard rules - which can be included in the source system quadrant, because all three layers conform to the source system data models - except for non information changing hard rules. Only the PSA being an additional layer that is being persisted. Considering the modern data platforms we could even use the PSA as replacement for the Datalake since it delivers similar purpose.

## Q3 - Source driven
### PSA - Persistent Staging Area
For common access and fast time to market we incorporate automated and delta-loaded persistent staging area. This includes a JSON based record format storage, that enables Schema on Read within the relational database. As mentioned before regarding the purpose this could also be seen as a replacement or duplicate of the classic Datalake:

- System of Record of the sources systems for easy access
- Easy automation - 1:1 transformation is easy to implement
- Schema on Read through JSON implementation in modern data platforms guarantees resilience towards schema changes
- Security net for the Q1 layer - history can be re-loaded into the Datavault layer in case of modeling issues

Through generated schema on read views access to the PSA is given to downstream processes within the relational platform. Schema on read enables resilience to schema changes of underlying source systems and independent release cycles as well.
### Hardrules
In preparation of the Datavault load processes we also introduce the Hard rules to keep unnecessary rule implementations for the Datavault automation layer away. From experience implementing re-structuring of source level datasets can include quite some transformation logic without really changing datasets or the core concepts of the logical model of the Datavault (ELM - CBC) they belong to. Often it is just lookup tables or other OLTP specific tricks that do not add to user understanding of the underlying information. Therefor it is worth a volatile prepare step, where those kind of rules can be applied - keeping the target model of the Datavault simpler and easier to understand.

Hard Rules are implemented as DBT projects - one DBT project per source system. If number of source systems are very high - groups of source systems can be build into one DBT projects. DBTs Analytics engineering methods enhance and augment the source delivery with documentation and source-based testing.

Other examples of hard rules are:
- Datatype consolidation - by default datatype conversion is implemented in generated Schema On Read views (SOR). Which build the input staging layer for the Hard Rule 
- Datatype related formatting rules (Date, Timestamp, Time)
- Adding information from the same source system that has multiple tenants or locations (UNION ALL)

Source specific logic that can not be implemented as interface within the source system, can be placed in the Hard Rules for that source system - in transparent and standard way like the soft rules using Analytics Engineering methods. But it should usually be preferred to have those rules incorporated into the source system. Reason being that release cycles and knowledge of the source system is usually separate from the data teams - which means maintenance of those rules should be handled in source system context more efficiently.
In this sense Hard rules should be seen as exception - or even a workaround - for when there is no other place to prepare the data before integrating it into the Datavault.

{{< figure src="/da_impl_overview.png" title="In-Database Layers" alt="In-DB Layers">}}

## Q1 Datavault
The facts of our data processing platform and the Q3 data quadrant is responsible for the non-volatile, subject oriented and time-variant storage of enterprise wide information - just like the classical enterprise data warehouse. Except that we use the Datavault modeling as physical model to 
- allow non-destructive changes
- to enable automation 
- and follow the model-driven requirements process introduced by ELM workshops.

Currently, for automation Datavault Builder and DBTvault (on Snowflake only) are supported for the datavault implementation. Both automation solutions have in common that physical patterns for data model and loading patterns are implemented in a way that supports 
- model-driven approach using business driven ELM workshops 
- and allow to use incremental implementation steps with ease and in compliance with the Analytics Engineering ideas.

Important asset of the Datavault layer is the information hiding through the ELM entity views. These are automatically created views that hide the Datavault logic to downstream processes in Q2 or Q4. Re-creating the ELM based views makes implementing Softrules or other downstream sandbox applications more stable and easier to understand. 

## Q2 Information Application
### Softrules
Softrules can only be applied downstream of the Datavault Q1 layer. The beautiful thing is, when a soft business rule changes, and they do change, data can be retrieved historically from the Data Vault and re-populated using the new rules.

This allows downstream models to absorb changing business rules. It also enables the business to non-destructively test proposed business rule changes.
### Mart applications
Most analytical applications that run frontends like Superset, Looker, PowerBI and others are and were using Star Schema or dimensional Models as their preferred source schema. 
Therefore our last step in the Dataflow in the pipeline is the Star Transformation. Separating Softrules from models that incorporate pure star schema model transformations is very useful, again by divide and conquer principles and modularization, this will make the single model files simpler to understand by users.

## Q4 Sandboxes
It is important to establish ground rules before opening the environment to user access. Starting point guidelines might include: 

- The environment is for short term studies and prototyping only, and is not to be used as a permanent home for applications or data
- Each user is assigned a controlled amount of storage, and governors are put in place to limit the computing resources they can consume
- Users must demonstrate both technical expertise and a willingness to abide by the environment’s rules before participating
- All sandbox users, and recipients of analyses produced in that environment, must understand that the analyses were produced without the level of validation and quality control provided by applications built on the “managed” portion of the data warehouse (Q2).

In order to keep the sandbox from becoming a permanent home for databases, some organizations run system reports on their tables to see how long they have existed. If something was built, say, six months ago, IT is justified in asking its owner what the purpose of the structure is and what the time frame is for the table to become a ‘certified’ piece of the data warehouse. 

In the end, the “democratization” of technology has made it likely that some end users will have significant database and SQL skills. It can be game changing for both these users, and their organizations, to provide them with a place to pursue data-based ideas and possibilities. However, these users must understand that they are working inside IT-supported environments.  In fact, we recommend that a written agreement between these users, their managements, and IT be put in place before they are granted sandbox access. This agreement should outline the ground rules for the database, the space to be granted to the user, allowed and disallowed activities, and any other guidelines that will make for a smooth-running environment. 

## Further reading
- [Dijkstra - Information Hiding and Software Engineering principles](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra)
- [Datavault Wikipedia](https://en.wikipedia.org/wiki/Data_vault_modeling)
