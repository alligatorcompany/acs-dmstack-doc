---
title: "02 Data Management"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Data Management

## Data Quadrant (4QM)
The field of data management is full of jargon and (mostly pushed by IT vendors) hollow narratives. Most business managers and executives have a hard time to fully comprehend the jargon and hence formulate a data strategy. Ronald Damhof developed the Data Quadrant Model to enable a company-wide discussion. As such, the Data Quadrant enables a common data language between executives, engineers, architects and legal experts.
{{< figure src="/dm_deployquadrant.png" title="Data Deployment Quadrant" alt="4QM">}}

| Quadrant | Description | Implementation folder |
| ----------- | ----------- | ----------- |
| Q1 | Facts - Subject oriented and Business Model driven implemented with automation | /01_dv |
| Q2 | Context - Softrules changing data as requested, implemented as DBT project per application / report | 02_app/<application-name> |
| Q3 | Sources - Source model driven Hardrules, one DBT project per source system - no changing of data - automation for ingestion and Persistent Staging Area (PSA) | 00_src/<source-name> |
| Q4 | Experimentation - Sandboxing for experimenting with data from all other quadrants - no rules except timer to end or transit to Q2 | 04_sbx/<sandbox-name> |


As organizations generate more and more data, they can no longer afford to be careless with it. Yes, data is a great asset for any organization in these modern times, but it's also a liability that needs to be managed and governed properly and fiercely. This balance between innovating with data and managing its liabilities is a prime directive of the Data Quadrant.
Within organizations, it's often about 'the single version of the truth', but this doesn't exist at all. There is a 'single version of the facts' and there are several 'truths’.

{{< figure src="/dm_pushpullpoint.png" title="Data Push Pull Point" alt="Data Push Pull Point">}}
The quadrants are vertically divided by the so called data push pull point - which is a reference to a concept from production processes. Comparing simple products that can be mass produced (like matches) and demand-driven products (like a luxury yacht). In the latter case production can only start once the customer order is known. If you have default parts like in a car the decoupling point lies in the middle between the first two examples. This can be compared to the data analytics field, where re-use should be incorporated from source data points being used in different information products.


{{< figure src="/dm_devstyle.png" title="Development Style" alt="Development Style">}}
Second differentiation is about the development style - the y-axis - which is
- systematic - user and developer are different people, hence you apply defensive governance aimed at control and compliance. In the pull site this is guaranteed by model driven automation and in the Q2 we use Analytics Engineering
- opportunistic -  user and developer are often the same person as in Sandboxes or Data Scientists. In these cases speed of delivery is essential and innovation needs to be possible. Hence we focus on flexibility and adaptability with loosened governance rules.

## Quadrant 1 - The Datavault

The single version of facts is the Datavault data integration layer, that should use model driven automation software. The model driven approach puts the target model into focus, that ensures business user involvement through the ELM workshops. Transparency in the communication between users and developers increase longevity and acceptance. Also the data delivery from the datavault layer simplifies Q2 context by basing those downstream business rules on agreed upon terms and topologies. Data reuse is enhanced for Q2 and Q4 information applications by using the same model terminology.
Through datavault patterns and the automation software the delivery of the Q1 is reliable and can achieve necessary scaling of processes.

## Quadrant 2 - The Context - Analytics Engineering

For the information product delivery - in order to achieve ease of use and user friendliness and low IT expertise we use Analytics Engineering, which focuses on SQL to describe transformations. Processes around versioning, platform management and services to deliver analytics frontends still rely on IT, but once in place a high level of self-service users can be achieved.
Domain expertise is the focus and different interpretations of the facts from Q1 can be delivered with high and reliable quality standards due to automated processes in the delivery pipeline.

## Quadrant 3 - Source Systems - PSA as system of record
In our data architecture the PSA stores the source copies of all source data - by the ingestion routines. In some organizations a data lake is being used to store a system of record for downstream application access to source data.

The quadrant of data sources is used in two scenarios:
- fast delivery for Q4 data experiments or sandbox's efforts
- Sourcing the facts in Q1. 
- System of record for reliability and debugging

## Quadrant 4 - Data Experimentation - Sandboxes
Data Experimentation of Sandboxes - like Data Science projects - often fail
- because of quality source data - which can be addressed by reuse of Q1 datasets
- because of restrictions in existing BI IT environments - hence we need Q4 with opportunistic development and offensive governance
- because business value is not being realized - insides in sandbox environments need to be monetized in productions situations. Therefor we need the elevation of Q4 information products into Q2

In the data architecture we also use Analytics Engineering as default for such experiments in sandbox environments. But following the opportunistic development approach it is important to ease these requirements and also allow for different styles. Sandboxes first-of-all are delivering data artifacts from all quadrants into the hands of the users without further restrictions except data access like PII otherwise regulated information.
## Data Governance applied

{{< figure src="/dm_governance.png" title="Governance Quadrant" alt="Data Governance">}}

Allowed data processes:
- Data flowing from Q1 into Q2 or Q4
- Data flowing from Q3 into Q4

Forbidden data processes:
- Data flowing from Q3 into Q2

Processes that should be enforced:
- Moving Q2 only applications into the Q1+Q2 model - there could by applications that actually hold their own data (planning tools, excel sheets) - those need to be sourced through Q1
- Information products in the Q4 should be elevated to Q2 to achieve economies of scale with data if you can generate and analyse it systematically - preparing it for enterprise-wide usage.

# Further Reading

-[Data Management on Datavault 1](https://prudenza.typepad.com/files/damhof_dbm0508_eng-1.pdf)
-[Data Management on Datavault 2](https://prudenza.typepad.com/files/damhof_dbm1108_eng2.pdf)
-[Data Management Quadrant](https://prudenza.typepad.com/files/english---the-data-quadrant-model-interview-ronald-damhof.pdf)
-[Artikel Data Management Quadrant](https://www.haefligermediaconsulting.com/files/media/files/79d87658b0a888c5529240b09d4f7746/ITL-FA_Data_Management_Quadrant_Netzwoche_04-2018.pdf)
-[Data Governance Wikipedia](https://en.wikipedia.org/wiki/Data_governance)
-[Data Management Wikipedia](https://en.wikipedia.org/wiki/Data_management)
