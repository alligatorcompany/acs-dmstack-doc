---
title: "01 Information Models"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Information Model

## Information model in the modern world
Already described in the introduction, the purpose of the information model is to describe the various information elements in technical and semantic terms.
Like any model, it only represents a part of reality, but it allows answering central questions around the information that an organisation or a person wants to use.
It serves as a knowledge map about the information elements - about data in existing information systems, as well about the data for systems being planned to build.
Both textual description und visualization are mecessary for both precision and ease of insight.


For example, having elements from the information model connected to elements in the implementations data model provide direct answers to questions like:
- is the implementation (data model) aligned with strategy (information model)?
- where are gaps or mismatches? 

Information models and data models are build to help understanding and communicating between different business and IT departments. Informationen models should be easy to understand and build.

Knowledge management introduced concepts like topic maps, semantic web (RDF) and the use of property graph to cope with connected information.
These proofed to provide viable approaches to information modeling and its network characteristic.

The challenges today are the increasing amount of data and stricter regulations. The number of connections in the internet age is increasing, as are regulatory requirements.

Because of the sheer information volume any type of modeling cannot not be done fully manually. For the same reason document based approaches do not scale.

## Data models

Where conceptual models are usually fairly easy, but often too far away from reality in IT systems. 
Logical models get too complicated because a lot of rules and business requirements are being represented by symbols for cardinality, optionality, abstraction and others. 
With the numbe of interconnected data elements these diagrams get pretty fast very convoluted. The diagramms alone do not suffice.

In the data domain the traditional IT came up with conceptual, logical, physical data models. Very often these were weak on the aspects of:
- glossaries
- connecting data elements
- edit a large number of model elements (bulk edit is essential)

### Conceptual 
Conceptual business object model can be a starting point for ELM. But from the analytics point of view, the logical view is the starting point to get our requirements.

Nowadays the term conceptual data model describes a type of model, that is to close to the logical model both in thinking and tool support. The tool support better matches the need of logical data modeling, but is often heavily lacking in its support for knowledge management and sementic description.

### Logical
ELM is more detailed adding business keys and the natural business relationships - which are important for analyzing data. Those NBR form along with ontologies in the business object model.

Dimensional modeling is the logical representation of a star schema as physical model. Having implemented the business target model for the data integration in the EDW - we will be able to build our dimensional model as re-organization of already existing ontologies and hierarchies from the edw.

### Physical Models
ELM maps directly into the physical model of the EDW: datavault

#### Datavault - Q1
Physical model is the implementation ready version of our information models - in our case build for DBMS. Datavault is the physical data model for the EDW - where star schema is one kind of physical model for the delivery layer. Using our ELM workshops, we should have an almost one on one fit for the Datavault implementation.

#### Star Schema & variants for Q2
The delivery or application layer can have different physical model implementations - depending on the requirements set by the receiving part. It could be star schema, unified star schema or flat table.
There used to be OLAP cubes in the delivery layer that persist multi dimensional models for specialized tools in the application delivery - often times used for planning. Usually we try to avoid this extra persistence component, because performance in modern DBMS is good enough when persists in star schema.

#### DBT
DBT is rendering physical model structure, where the focus is on transformation, building modular building blocks for your transformations that happen downstream of the EDW/Datavault. In the Datavault context this is known as Softrules. Source driven transformations are upstream of the Datavault and called Hardrules. In Hardrules data cannot be change through transformations, but DBT artifacts can therefor mostly be generated through dbt-dwhgen.

# Further Reading
- [Data Modeling Library 2022](https://modelyourreality.substack.com/p/a-data-modeling-library-for-2022)
- [Wikipedia Overview for Data Modeling](https://en.wikipedia.org/wiki/Data_modeling)
- [Global Data Management Community and the Body of Knowledge DMBOK](https://www.dama.org/cpages/body-of-knowledge)
- 