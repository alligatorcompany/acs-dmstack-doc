---
title: "Ensemble Logical Modeling (ELM)"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Ensemble Logical Modeling (ELM)

Mapping the organization has been part of IT efforts for a long time and resulted in quite a few ways of enterprise modeling, business process modeling or data modeling methods - but all mostly IT-driven. 

In the data analytics and warehousing space these are 3NF variants, Star Schema models and similar type of mostly physical models. During the last two decades new forms of physical models have occurred in the data space - all could be seen as similar in some ways. They also follow the concept of divide & conquer - to divide changing from non-changing parts of the original model forms. This is the 
- business key
- relations
- attribution

{{< figure src="/elm_emp.png" alt="Ensemble Logical Model">}}

Therefor Geneseeacademy - namely Hans Hultgren - together with Lars Rönnbäck, Patrick Lager came up with the ensemble modeling pattern.

Afterwards the idea was born to combine the similarities in a common logical model framework with a little bit different approach compared to previous data modeling efforts - business workshops as requirement gathering sessions instead of modeling complex IT structures with cardinalities, crow feet, optionality, and so forth.

These workshops are setup with business matter experts, a facilitator and a scribe. flipchart, sticky notes (like in Datavault CDVDM), and paper based forms should be enough tooling to conduct those sessions.

## Short intro
The sticky notes being

- blue for identifier or instance of Core Business Concepts - CBC
- green for Unique specific Natural business relation - NBR
- yellow the attribution, context describing the CBC

The Ensemble Logical Model is for communication
- for business representatives - their own business representation using their terminology and business objects
- data analysts - to understand and recognize the terminology, relations and their definitions and descriptions
- source systems - through mapping - they can provide needed information
- for developers - an almost 1:1 translation towards one of the Ensemble Modelling Patterns (EMP) above

The forms being
- The CBC list - initial list of all CBC categorized into Event, Person, Place, Thing and Other
- CBC Canvas - CBC types for Event, Person, Place, Thing - discussing duplicates and missing CBC
- CBC Form - Definition, business key, description and attribution of a CBC
- Event & NBR Canvas - Designing and refining relationships
- NBR Matrix - Listing all named business relation ships and their participants - good hand-over for dimensional model implementation
- NBR Form - description and grain verification

ELM as logical model specialized for the EDW, close to business process to talk to business people in their comfort zone. Transparency by using the business community language for implementing the EDW entities. Data profiling sources helps the requirements process, but should not be the driving factor.

## The ELM workshop process

1. Identify and Model the Core Business Concepts
- What does your organization/department do? 
* Elevator pitch (keep it short and to the point)
* Explain to nitwit (10 yr old)
- What happens in your organization/department on a given day?
- What do you think other departments / people in the organization tell me what your department is doing and/or is responsible off?

2. Identify and Model the Natural Business Relationships
- What is the relationship between the CBC’s
- How do we know? 
- Is this relationship a driver for your organization/department?

3. Adding Attribution on CBC form

## NBR Design

In NBR/Link design, we have different concepts in different styles of Datavault (Keyed Instance, Transaction design, Unit of Work, Binary link design). In ELM, we use the NBR concept, that depicts the natural grain of the relation between CBC. Additionally Keyed Instances will lead to links with more than two CBCs involved. Some Datavault implementation vendors on the other hand are using the binary link design only. Datavault Builder is one of them. It is important to note, that if implemented correctly, binary links can always be resolved / aggregated into their corresponding 3- or 4- way links. Therefor from our experience it is no issue to go from logical NBR into a physical implementation with binary link design. One advantage of binary link design is, that you will see issues, if NBR design by accident was off because of too many CBCs involved.

## Backbone Model - Handover Development

The backbone model is the main outcome and handover from the ELM workshops to the implementation.
{{< figure src="/elm_backbone.png" alt="ELM Backbone Model">}}

**Resulting Story or Epic should include the following:**
1. Report Requirements
    * Visualization or Prototype: Screenshot with comments
    * Questions: Open question list
2. Data Requirements (ELM)
    * Enterprise Data Story: short business description of what information is to be shown or delivered
    * ELM Model: Screenshot of the ELM to be implemented and the CBC list with CBCs and descriptions
    * Dimensional Model: Fact Table grain and their dimensions - Screenshot of PostIt model
    * links to the ELM artifacts from the workshop
3. Functional Dataflow
    * Interfaces: List of sources, the source interfaces and their technical keys (might be different from business key), that need to be imported and special hints how to access them
    * Golden Source Rules: In case of multiple sources being integrated we need
    * Hardrules: special Hardrules if necessary (Datatypes are being conformed automatically). Remember those should be exceptions and not change any datasets contents
    * Mapping and Softrules: any rules that need to be applied before delivering the mart schema or report


Along with the general, written requirements and the ELM artifacts, developers have a good chance to understand the business scope, make informed decision during implementation and can start good conversation with business users.

This reduces cycle time for your requirements by a leap.

## Naming standards

Logical names from ELM workshops with business users may get into conflict with implementation in Datavault automation software or what the DBMS vendors allow for object names. The guideline here is to be most transparent and present those issues upfront with the business users, using these guidelines - try not to discuss. Also, stay on the level of language and names - do not dive into technical detail - because this can only help with confusion.

### Subject Area
Make sure you have a subject area list in place, that depicts business process areas of you organization and not functions or departments. All CBCs should be placed into one of those subject areas.
### CBC and NBR
First of all The names for CBC, NBR and Attributes, should be common language, that all participating workshop members immediately understand and can agree upon. Since we are implementing Datavault from the backbone model from CBC and NBR 

Agreement how to deal with the following issues helps:
- Snake_Case or CamelCase naming to resolve conflict if spaces are issues
- Avoid non ascii characters if possible - "umlauts" should be acceptable
- Length issues - although DBMS vendors raised the limits - due to automation software concatenating names e.g. in links - these limits are reached faster in Datavault. Share necessary abbreviations with the business

Logical names from ELM workshops with business users may get into conflict with implementation in Datavault automation software or what the DBMS vendors allow for object names. 

Most of the naming issues become less of an issue, once you have entity views in place, that only deliver the general backbone model with CBC names. If you have modeled your transactions and keyed instances as Hubs,  NBRs/Links will be hidden behind those views, too. The only names are in the attribution.

Most of the naming issues become less of an issue, once you have entity views in place, that only deliver the general backbone model with CBC names. If you have modeled your transactions and keyed instances as Hubs,  NBRs/Links will be hidden behind those views, too. The only names are in the attribution.

With Datavault Builder you can have logical and physical names. Use this on Hubs, Links and Satellites. In DBTVault we rely on DBT.
### Attribution

All attribute names used in star schema models should be base on business terminology. Since we follow so call late binding for attributes, the rename of attribute names should be conducted in Softrules in DBT. Here we have best visibility and all changes for names will be in the same place.

We recommend the following standards for attribute names:

A) Use snake_case for all names and avoid non ascii characters if possible (I cannot believe, this is still necessary).

B) Choose appropriate suffix for the attributes:

1. Key fields:
- identifier
- code

2. Timestamp, Date, Time
- date
- time
- timestamp

3. Boolean / Flag
- indicator
- flag

4. Decimal and monetary: 
- amount
- number
- quantity
- rate

5. Other
- name
- description
- image
- sound
- text

# Further Reading
- [Training](https://www.geneseeacademy.com/)
- [Brief Introduction](https://www.linkedin.com/pulse/brief-guide-ensemble-logical-model-remco-broekmans)
- [ELM video](https://www.youtube.com/watch?v=hl71R2d4_FU)
