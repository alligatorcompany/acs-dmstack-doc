---
title: "03 Implementation"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Implementation

In this chapter the general principles of Datavault architecture implementation from different Data

1. PSA - technical, automated storage of source data without any transformation. Used for fast track Sandboxing and can be a valuable security net before building your first Datavault. 
2. Datavault for long term storage of integrated data organized by a business driven target model (ELM)
3. Dimensional Model - Star Schema implementation is still most popular both for frontend usage and the easiest to produce downstream of a Datavault, since the business ontologies, that are going to make up our dimensional hierarchies are already implemented in the target model based Datavault.
