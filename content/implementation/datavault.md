---
title: "Q1 Datavault"
date: 2021-11-23T03:01:15+01:00
draft: false
---
## Brief introduction into Datavault
The Datavault was invented by Dan Linstedt in 2000 as a physical data model for the enterprise data warehouse (EDW). The Datavault differs mainly in two aspects as compared to the other two dominant EDW practices in place at that time 
- 3NF (Bill Inmon) 
- and Dimensional (Ralph Kimball).

Firstly, Datavault stores all data - all facts in the data warehouse, in contrast to seeking for the correct dataset before integration and historization (single version of truth vs. single version of facts). 

Secondly, by separating the relational parts of an entity (key, relation, attributes) into their own physical tables (Hub, Link, Satellite), he created a non-destructive data model, that greatly improves durability and also performance in some ways. 

Performance gains may occur counterintuitive at first glance because this will introduce many tables and joins, but the pattern around Hub/Link structures with short rows and predictable join paths does perform well on modern DBMS. Also, we need to differentiate between performance to store the data and to retrieve the data from the Datavault. The architectural view of Datavault is important and that is optimization for long-term storage of time-variant, integrated data and concerning retrieval. 

Retrieval of the data is a separate concern, that should be looked at in isolation (see Dimensional).

## Divide & Conquer
In the endeavour to simplify requirements gathering and bring business people on board for the Datavault, an Ensemble Logical Model (ELM) was created. This is a 1:1 abstraction for a logical model and workshop artifacts for requirements gathering.

An ensemble is a Core Business Concept (CBC) with
- business identity in a Hub
- context attributes describing the CBC in a Satellite
- relations to other ensembles in a Link

We are separating those parts into separate physical tables in order to get benefits for long term storage of information:
- non-destructive data model evolution
- extendible
- only three ever recurring load patterns

Those three recurring load patterns for hub, link and satellite are key to enable easy automation and minimal technical debt in the EDW data integration pattern.

We also divide & conquer on the data integration and transformation level by separating the data architecture into
- Q3 source driven Hardrules - source based corrections or transforms before the Datavault - never changing contents of datasets - only structure
- Q1 Facts - Integration and Historization, the Datavault
- Q2 Context & Interpretation - Soft Rules, that are business rules for downstream applications

It is important to note, that Datavault is not contradicting 3NF as a normalized form of storing relational, granular data. In contrast it is basically conform to most features of 3NF and can be transformed without any data loss:
{{< figure src="/dv_3nf_key.png" alt="Datavault 3NF">}}
The Datavault compared to other normalized models is
- true to the key with all attributes in the Satellite being dependent on the key in the Hub table
- adding more normalization by factoring out Satellite attributes (yellow) and relations (green) in Link tables

### Data Integration with Datavault
Datavault enables automation for the enterprise warehouse because of the use of patterns.

These load patterns ought to be the same for the whole Datavault DWH and should never be altered. By the combination of Hub and Link structures we have a self-repeating structures that in total build a network structure, that can be easily maintained and queried by the DBMS engine. Of course for human eyes it might be difficult to see, but utilizing visualizations and ELM, we can comprehend it on an abstracted level. The physical implementation is mostly suited for the machine.

#### Loading a Datavault
Additionally those load patterns being executed in this network are restartable and delta-enabled. Therefore we do not need to differentiate during load execution for this. Based on pure SQL they also support modern DBMS with clustered infrastructure very well.

Scheduling workflows for the DWH becomes therefor very easy for a Datavault:
{{< figure src="/dv_loadpattern.png" alt="Datavault Load Pattern">}}

#### Integration datasets from multiple sources
Hubs and Links are the main integration points where we can combine information from multiple source systems.
{{< figure src="/dv_int_hub.png" title="CBC, Hub integration" alt="Datavault Hub Integration">}}

{{< figure src="/dv_int_link.png" title="NBR, Link integration" alt="Datavault Link Integration">}}

### Think in increments
Due to the non-destructive data model we minimize test efforts and keep stable for virtualization. 
When modeling datavault / ELM CBC - always think about the Hubs as extension points for future requirements. It is worth implementing a hub even if it has not much further context to add momentarily. It can have more attributes later - and we also are ready for additional links adding to our model.

## Datavault in practice
### Business Driven Modeling
The datavault should be build by a business target model, that does not derive or is based on the data models of source systems. The Datavault should be a source of finding and retrieving data for the users - the naming and structure should be based on the user terminology and business processes. It is derived from those business objects, CBCs and NBRs when using ELM, that build a long-term and transparent basis of our Data warehouse.
{{< figure src="/dv_businessdriven.png" alt="Business Driven Target Model" >}}

Using this target model chances are higher that the Datavault can continue to be used including all downstream processes, even if source systems change or are replaced.
User transparency and involvement also adds trust into the data team.

Another aspect of this modeling is the incremental evolvement of the model - without introducing changes into the existing model - or non-destructive data model. 
{{< figure src="/dv_increment.png" alt="Datavault Incremental">}}

Adding elements from a new subject area can always be introduced into the model, granted there is a Hub, that can be imported into the additional link. Existing structures have no change and we need no re-testing of the complete existing model prior to this change.

### Easy Data Retrieval - Information Hiding
As architectural design pattern information hiding is important in software systems, in order to hide internal logic of a module from others that use its API or functionality. Therefor independence is higher and internal module changes have no downstream effects. These principles can also be applied in our Datavault data architecture by providing views as an API or interface for downstream processes. These views will
- hide internal join logic how hub, link and satellite might be related in order to retrieve the CBC dataset
- incorporate the logical model terminology to easy be recognized by users that try to get data from the Datavault
- can include historical or current information regarding the CBC attribution (SCD2 possibly)
{{< figure src="/dv_entityview.png" alt="Datavault Entity View" >}}

In above figure the entity views are simplified to showcase the general idea. In reality there might be multiple versions of those entity views per entity, depending on the timeline - load time or others. But these views can be generated by the automation software (in case from Datavault Builder) or being generated by an algorithm called Supernova - that uses the patterns used in Datavault, to generate those views including history automatically.

{{< hint info >}}
**Introduce project specific naming conventions for entity views**
For transparency and good maintenance it is good practice to introduce naming conventions that mark different views per entity based on their interpretation of the timeline: e.g. ORDER_CURRENT vs. ORDER_HISTORY or a nomenclature known in the dimensional world with as-is vs. as-was interpretation. 
{{< /hint >}}

Having those entity or CBC views in place, will greatly improve user acceptance when working on downstream data applications, that need datasets from the Datavault. They also will have impact on development speed and quality, since there is no need to build knowledge about Datavault query logic across the board.

### Datavault Patterns

#### Only add elements
Even if you just add one attribute to a pre-existing CBC - add a new Satellite for that attribute. Same is default for changing NBR / link granularity. If you need to split a header/detail event into two links because of parts that show redundancy, create two new links and keep the old one in tact. Switch your entity views to correctly use the newly created links. This way you can always use old artifacts for comparison.

Of course there can be points reached where a lot of deprecated links or defragmented satellites will call for refactoring because of too much clutter and there is a danger of over-complicating the model. In this case it is valid to call for refactoring User Stories to 
- re-combine Satellites from the same sources
- remove old, deprecated links or hubs
- move Hardrules and Softrules around

But for the sake of incremental and fast delivery, these should be exceptions and also properly communicated, because those User Stories will not add immediate business value.

{{< hint info >}}
**Valid Exception: Elements that have not been released to production**
The only exception to this rule is if you are working on parts of the model, that have not been published to production, yet and you can be sure to get ahead of the deployment pipeline in order to guarantee your changes will be in place.
{{< /hint >}}

Even for refactoring and if you have a PSA in place, together with the corresponding data migration script for initial loading, do not change - only add the new Datavault elements. Mark the old once deprecated and make sure your entity views point to the new elements. Looking backwards, there are always changes, you might wish to see this old implementation again for comparison.

#### Business Key - Enterprise wide?
Unfortunately, in reality of organizations, the enterprise wide business keys, that are known and used by the business users and also maintained throughout the system landscape of IT, are rare  beasts.

Instead we often find system generated keys or also compound keys that consist of sub-keys or relation keys that themselves are again system generated.

Due to this reality, we suggest to use those keys for practical reasons before we cannot integrate the data because of lacking existence of this enterprise wide business keys.

For this reason, in hub implementations, the business key should be a text column, that can 
- implement any kind of business key
- cope with changing business key
- and allows for compound business keys as concatenated string

This way the physical implementation of our Datavault hubs can stay unchanged even if CBCs later in the lifecycle of the DWH project will change their business key - e.g. when source systems change.

#### Transactions & Events
Events are best modeled as hub using their natural business key or compound key made up from the participating CBCs. Looking at different kind of events, we see common patterns, that repeat often and can therefore be used as stencil for your implementations:

{{< mermaid >}}
graph TD
    classDef link fill:#51af27,stroke:#51af27,color:000
    classDef hub fill:#6baeff,stroke:#6baeff,color:000
    classDef sat fill:#ffde24,stroke:#ffde24,color:000

    p1(Store):::hub --- l1(H):::link
    p2(Customer):::hub --- l1(H):::link
    p3(Employee):::hub --- l1(H):::link
    l1{H}:::link --- e1(Order):::hub
    e1(Order):::hub --- l2{D}:::link
    l2{D}:::link --- t1(Product):::hub 
    t1(Product):::hub --- f3(Code):::sat
    t1(Product):::hub --- f4(Name):::sat

    head1(Webstore):::hub --- l3(H):::link
    head2(Customer):::hub --- l3(H):::link
    head3(Employee):::hub --- l3(H):::link
    l3{H}:::link --- e3(Repair):::hub
    
    e3(Repair):::hub --- l4{M}:::link
    l4{M}:::link --- thing1(Part):::hub
    thing1(Part) --- f5(Part Name):::sat

    e3(Repair):::hub --- l5{S}:::link
    l5{S}:::link --- person1(Mechanic):::hub
    person1(Employee):::hub --- f6(Mechanic Name):::sat
{{< /mermaid >}}

- "Dog Bone": Events with typical Header/Detail structure like Order and Order line

- "Wish Bone": Events with multiple details that involve different CBC - like a bike repair, that can involve parts and service hours from an employee.

#### Keyed Instance

The "Keyed Instance" (KI) is a CBC that represents the link level granularity to add descriptive attributes at the level of the NBR / Link. From above Order event example, we have two Keyed Instances. The Order - that is "naturally" at the granularity of the "Header (H)" link, because the rule says that events should be represented by a CBC/Hub. But additionally we will add a detail level keyed instance, in order to add attributes that describe the "Detail (D)" level where we need to describe how many and at what price the products had been soled:
{{< mermaid >}}
graph TD
    classDef link fill:#51af27,stroke:#51af27,color:000
    classDef hub fill:#6baeff,stroke:#6baeff,color:000
    classDef sat fill:#ffde24,stroke:#ffde24,color:000

    p1(Store):::hub --- l1(H):::link
    p2(Customer):::hub --- l1(H):::link
    p3(Employee):::hub --- l1(H):::link
    l1{H}:::link --- e1(Order KI):::hub
    e1(Order KI):::hub --- l2{D}:::link
    l2{D}:::link --- t1(Product):::hub 
    t1(Product):::hub --- f3(Code):::sat
    t1(Product):::hub --- f4(Name):::sat

    l2{D}:::link --- t2(Orderline KI):::hub 
    t2(Orderline KI):::hub --- f5(Amount in EUR):::sat
    t2(Orderline KI):::hub --- f6(Quantity):::sat    
{{< /mermaid >}}

Having the detail level Hub in place will guarantee extensibility, because if we need to add a return, complaint or delivery of a product on an order, we need to link to the specific item on the orderline level.

{{< mermaid >}}
graph TD
    classDef link fill:#51af27,stroke:#51af27,color:000
    classDef hub fill:#6baeff,stroke:#6baeff,color:000
    classDef sat fill:#ffde24,stroke:#ffde24,color:000

    e1(Order KI):::hub --- l2{D}:::link
    l2{D}:::link --- t1(Product):::hub 
    t1(Product):::hub --- f3(Code):::sat
    t1(Product):::hub --- f4(Name):::sat

    l2{D}:::link --- t2(Orderline KI):::hub 
    t2(Orderline KI):::hub --- f5(Amount in EUR):::sat
    t2(Orderline KI):::hub --- f6(Quantity):::sat    

    e2(Return):::hub --- l3(R):::link
    l3(R):::link --- t2(Orderline KI):::hub
    e3(Complaint):::hub --- l4(C):::link
    l4(C):::link --- t2(Orderline KI):::hub
    e4(Delivery):::hub --- l5(D):::link
    l5(D):::link --- t2(Orderline KI):::hub
{{< /mermaid >}}

And all these additional events can be added anytime without changing the existing Orderline event.

{{< hint info >}}
**Use Keyed Instances instead of Link Satellites**  
In earlier versions of the Datavault standard, link satellites or link on link implementations might have been described instead of Keyed Instances. Also there are still discussion about the need of link satellites being necessary because of performance reasons. We are aware of those discussions and agree that there might be cases where physical implementation is not feasible, but then these would be exceptions. Default should be to prefer extensibility and future proof model implementation (see also anti-pattern link satellite for more information). 
{{< /hint >}}

#### NBR

When designing an NBR, we always need to focus on the "Unique, Specific, Natural Business Relationship". This means
- Think of all CBC that come together being part of an event first - second fill out sample records and see wether you can see sparsity or redundancy - also include the business department.
- Do no tbe shy to make multiple NBR in one scenario if you see redundancy or sparsity. Having NBR in place that represent each individual kind of Business Process variant in separation is going to make your Datavault model more readable. People will be able to select from the correct table, if naming is familiar. If necessary datasets can be combined again later on 
- Always include the business department in naming and granularity discussions. 

{{< hint info >}}
**NBR and Datavault automation**
There are automation implementations (namely Datavault Builder but also others), that have decided to implement binary links only, in order to be as granular as possible. It is important to understand that if those implementations have proper status tracking for there load processes, details or less specific combinations can always be re-constructed from those binary links. We still recommend to have NBR designs on the logical level, regardless of how your Datavault implementation to give guidance for read-out processes.
{{< /hint >}}

### Datavault Anti-Patterns
#### Transaction as Link & Link Satellites

As of 2017, the DVEE voted to provide the guidance that link satellites should not be used. Consequently, events have to be modeled as Hubs in order to store attributes. Hence the discussion of eliminating transactional links (for events including link satellites) and whether to have link satellites at all is related. Same as the Keyed Instance Hub - that if not naturally created as CBC due to the event being a CBC and a Hub - which is the link granularity (1:1) representation in an NBR, that can store the relation attributes. Experience told us that once you have attributes to store on the event level - what you have is a CBC - which is not relationship only. Therefor our understanding is that from the Datavault pattern point of view there is no real discussion whether there should be link satellites or not - because links are "just" relations and only CBC have attribution. 

#### Fact-based link & Dummy Records

There is a phenomenon that is basically an overly wide link construct. Usually - if a link has more than 3 links - it should be worth it checking, wether the link is actually without sparsity. In star schema implementation, we are used to add dummy records for dimensions, that cannot be filled for a certain record in the fact table. Which for performance reasons is a good solution in the star schema, should be avoided in the Datavault model. Usually, too wide links carry multiple variants of business processes (orders with or withour customer reference - in-store or online) - where we know our customer or not. Those are two separate events, that might be in scope for different analysis. If we store all records in one big link, we need to add logic to our query to separate those two events. In case of two separate links - we choose by joining one or the other.

{{< mermaid >}}
graph TD
    classDef link fill:#51af27,stroke:#51af27,color:000
    classDef hub fill:#6baeff,stroke:#6baeff,color:000
    classDef sat fill:#ffde24,stroke:#ffde24,color:000

    p1(Store):::hub --- l1(L):::link
    p2(Customer):::hub --- l1(L):::link
    p3(Employee):::hub --- l1(L):::link
    l1{L}:::link --- e1(FactLink Order):::hub
    p5(Webshop):::hub --- l1(L):::link

    p6(Store):::hub --- l2(H):::link
    p7(Customer):::hub --- l2(H):::link
    p8(Employee):::hub --- l2(H):::link
    l2{H}:::link --- e2(StationaryOrder):::hub

    p4(Webshop):::hub --- l3(H):::link
    l3{H}:::link --- e3(OnlineOrder):::hub

{{< /mermaid >}}


The other example would be redundancy - like in the order/detail scenario above. If we did not separate out the Orderline into its own event, we would have only one, big event with one link:
In this case the order header attributes are redundant - as they are the same for every ordered article on the receipt. 

#### Multi-active Satellites

Multi-active Satellites are not part of the Datavault pattern. It just changes the Satellite pattern by introducing additional granularity with another key - which makes the attributes not only dependent on the Hub key. Additional load patterns need to be introduced and also reading from the Datavault is not as easy anymore - because those multi-active satellites have to get special treatment here as well.
One concept that is also part of the CDVDM training is context close to key. Which basically underlines this anti-pattern and experience tells us that especially in the context of analytics - context is never unbounded. This means we can always introduce multiple set of attributes for multiple instances that occur. Most famous example being the address as delivery address, invoice address and home address. 
{{< figure src="/dv_nomas.png" alt="Datavault No Multi-active Satellite">}}
Multi-Active Satellite (MAS) is mixing colors (comparing to 3NF models having keys, relation and attributes in one entity - see above).

Another example usage of MAS is bi-temporal storage of data - adding an additional timeline attribute to the key of the satellite. Instead of using this pattern in the Datavault directly, taking the disadvantages of changing the load and read patterns, we suggest do keep the timeline attributes in the satellites without adding them to the key. Usually, since Datavault is loaded in batch, micro-batch or streaming sources, we do have single instances per load time and could also store multiple additional timeline attributes in parallel first - using the standard patterns. Loading the data from the Datavault those attributes can be interpreted to build bi-temporal constructs outside the Datavault. This is also inline with the idea of Divide & Conquer - keeping Datavault load and read simple - leaving bi-temporal logic to the Softrules outside.

### Business Datavault
From our approach with ELM and business model driven Datavault, the separation between Raw and Business Vault actually looses focus. Since we have a business target model, everything in the Datavault is Business Vault. If we have to load Datavault constructs, that are not directly related to the ELM, that is to what the business told us to model, then these are exceptions and can be tagged as Raw Vault.
{{< figure src="/dv_raw.png" alt="Datavault Raw As Exception">}}

In above example, the business told us, that there is delivery information on the level of each item of a sale. But later, when looking in the source systems, we had to learn, that the source system is planning to implement this, but only next year. The only information currently available is the delivery for the Sale. In this scenario we can already model the Datavault as anticipated by the business users in the ELM workshop, but until the source system has changed, we cannot have the detailed link loaded. Therefor the Link between Delivery and Sales is a Raw element, because it contains information as the source has it. Once the source system delivers the line item level delivery information, we can load the additional link.

{{< hint info >}}
**```Raw Vault``` and ```Business Vault``` ARE NOT two things - there shall be only ONE Business Driven Datavault**

Remember, wether the **Datavault** is sourced from a source system or from rules you have to add in Q2, your Datavault should be one **Navigational Map** to help your end users locate there data like from "one single mould".
{{< /hint >}}

In the Datavault Satellites, Links or even Hubs can be loaded from Soft Rules. In this case we just add a loop over the load pattern and source the Softrules views as Datasets for our standard Datavault load patterns. Thanks to the virtualized Entity Views and standard load workflows, the derived and augmented (DEA) attributes, links or hubs - automatically become part of the outgoing interface - ready to be used by downstream applications.
{{< figure src="/dv_bv_load.png" alt="Datavault Business Vault Load">}}

## Datavault Builder

### Use partial Deployment in DEV 

Working on a User Story it is good practice to only import parts of the model into your disposable development environment. 

When your additions are finished you can export the DVB deployment and version changed artifacts with your commit. Keeping this export small will also make it easier to create the most specific commit that contains only changed json files from the DVB model.

The following changes can be safely ignored:
- Column orders within objects
- Attributes in the json that are null, empty or "unknown"
- Colors of source systems

### Use PSA and BDV layers in Datavault model

Good practice is to mark raw elements that are auxillary or not even part of the ELM to be PSA - not only loaded directly from source as Raw vault suggests, but also not even the real target model.

Secondly, those parts of the Datavault, that  are loaded from Softrules via the DBT based load, should be marked as Business Datavault.

This will enhance transparency within the Datavault Builder and make navigation even easier.

### Entity views from dimensional model

In Datavault Builder the entity views to interface your Datavault for your Softrules are implemented as the so called "Dimensional Model". But instead of directly building the dimensional model within the Datavault Builder, suggestion is to only build the entity views that represent a CBC plus relations through links using the Hashkeys.

That way you get a target model based entity views including their dependencies. This is a good representation of your ELM backbone model, that can be used to build your Softrules and the 

Business Rules are not used within Datavault Builder, since this is where DBT enhances documentation, testing and transparency over rules that make up the report or analytics models.
### Automatic Job Creation

It is preferred to have automatic job creation only in Datavault Builder. That way we can simplify the Dataflow and not worry about dependency management within the Datavault.

The only distinction is source-based Hardrules and Softrules based Business vault parts, that are being executed using the corresponding job lists and model selections in the Softrules DBT projects.

## Further reading on Datavault
- [DVEE community driven Datavault Standards - organized by GeneseeAcademy](http://dvstandards.com/about/)
- [Blog from the inventor of Datavault - Dan Linstedt](https://danlinstedt.com/)
- [Datavault on Wikipedia](https://en.wikipedia.org/wiki/Data_vault_modeling)
- [German Datavault User Group](https://datavaultusergroup.de/vortragsarchiv/) 
- [Turning EMP normalized pattern links into speedups - by Lars Rönnbäck](https://www.anchormodeling.com/wp-content/uploads/2011/05/Turning-Horrible-Joins-into-Wonderful-Speedups.pdf) 
- [Datavault Hans](https://www.amazon.com/Modeling-Agile-Data-Warehouse-Vault/dp/061572308X) 
- [Elefant in the Fridge](https://www.amazon.com/Elephant-Fridge-Success-Building-Business-Centered-ebook/dp/B07RH9C2JK) 
