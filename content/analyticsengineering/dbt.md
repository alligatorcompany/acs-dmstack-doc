---
title: "01 DBT"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Data Build Tool

## Analytics Engineering
With the rise of easy accessible cloud warehouse solutions and their relational engines implementing horizontal scaling features (MPP), transformation logic becomes more often implemented in SQL and the architectural style of Extract Load Transform is gaining popularity again. Loading raw data into the DBMS first and then performing transformation logic utilizing the DBMS engines and SQL formulating those transformation's had and still has advantages:
- SQL as descriptive language is easier to write - as compared to MapReduce algorithm or 
- Better utilization of infrastructure - as compared to legacy DBMS (DB2, Oracle, Sybase, Informix)
- Less need to understand MPP inherently as compared to Hadoop (MapReduce) or older relational engines (Teradata DBMS v16.20 and earlier, Redshift)
- Transformation logic and documentation in code (SQL and YAML) are easy to maintain in code repositories (git)
- No additional infrastructure for transformation logic as compared to ETL architectures

But the criticism from older days discussing ETL vs. ELT remain true today:
 - how to organize the SQL code?
 - should we use stored procedures?
 - how do we test this code and make sure it is maintainable?

Native solutions for code in the DBMS have not improved much over the years. There are tools from some vendors, but usually those are tightly integrated with one dbms product (vendor lock-in) and did not (yet?) adopt the modern practices and methods found in DevOps / DataOps.

The Data Build Tool (DBT) comes to the rescue by introducing software engineering practices into the game of data transformation. DBT has been developed from the start on those DBMS (Snowflake, Postgres, Redshift and BigQuery). Transformation rules formulated in SQL become modularised, testable and thus more reliable. Just by introducing a development flow, that imposes software engineering practices into the data warehouse and data integration world.

- Modularizing transformation logic into models that are organized in a direct acyclic graph (DAG) through easy references and nicely visualized
- Test driven design
- Reusable code
- Less vendor lock-in

Additionally, todays users of analytics applications have become more tech-savvy. An increasing number can read or also write SQL transformation logic.
This way old roles seen in companies implementing data analytics teams are changing as well.
Overloaded data engineers are no longer responsible for the whole data stack end-to-end.
Analytics engineers, Data Analysts and Report Users can contribute to a larger part by using SQL instead of specialized data transformation tools.
Ingesting raw data, maintaining the data warehouse platform and performance on a technical level is more separated from transformation logic and business knowledge that needs to be curated, documented and tested by business matter experts.


## DBT good practices

### Default configuration

#### DBT project setup in a Monorepository

By default the [DBT profiles](https://docs.getdbt.com/dbt-cli/configure-your-profile) are located in the user home folder or as defined by the `DBT_PROFILES_DIR` environment variable. In order to support the Monorepository format with multiple DBT projects in one repository (PSA, Hardrules and Softrules), the profiles are copied into every DBT project base directory and we set the `DBT_PROFILES_DIR=.`.

- [profiles.yml](https://docs.getdbt.com/dbt-cli/configure-your-profile):
```yaml
dbt:
  target: dev
  outputs:
    dev:
      type: exasol
      threads: 1
      dsn: "{{ env_var('EXASOL_SERVICE_HOST') }}:{{ env_var('EXASOL_SERVICE_PORT') }}"
      user: "{{ env_var('DBT_USER') }}"
      pass: "{{ env_var('DBT_PASS') }}"
      dbname: dbt
      schema: ZZZ_Q3_SRC

    ci:
      type: exasol
      threads: 1
      dsn: "{{ env_var('EXASOL_SERVICE_HOST') }}:{{ env_var('EXASOL_SERVICE_PORT') }}"
      user: "{{ env_var('DBT_USER') }}"
      pass: "{{ env_var('DBT_PASS') }}"
      dbname: dbt
      schema: ZZZ_Q3_SRC

config:
  send_anonymous_usage_stats: False
```

As you can see we use two targets - usually with the same parameters, in order to switch between Seed sources for CI and PSA source in regular case. The base schemata are

- YYY_Q3_PSA - for PSA
- ZZZ_Q3_SRC_<<suffix>> - for Hardrules
- AAA_Q2_APP_<<suffix>> - for Softrules / applications
- AAA_Q4_SBX_<<suffix>> - for Sandbox projects

Every specific project defines a schema suffix to get to the final schema name per dbt project.

In the project yml, we add the tag to tell whether the project is on Q3 Hardrule, Q2 Softrule or Q4 Sandbox layer (hardrule,softrule,sandbox). Also we add a schema suffix specific to every project:
- [dbt_project.yml](https://docs.getdbt.com/reference/dbt_project.yml):
```yaml
name: 'fifa'
version: '1.0.0'
config-version: 2

profile: 'dbt'

models:
  fifa:  
    +tags:
        - 'softrule'
    schema: fifa

dispatch:
  - macro_namespace: dbt_utils
    search_order: ['exasol_utils', 'dbt_utils']
```

- [packages.yml](https://docs.getdbt.com/docs/building-a-dbt-project/package-management/):
```yaml
packages:
  - package: dbt-labs/dbt_utils
    version: 0.8.0
  - package: dbt-labs/codegen
    version: 0.4.1
  - git: "git@github.com:tglunde/exasol-utils.git"
  - local: "{{ env_var('DBT_BASE_DIR') }}/02_app/OtherSoftruleProjectImported"
```
Additionally, in order to support different basedir locations within the Monorepository, we need the `DBT_BASE_DIR` variable to point to the project base directory - which usually is also set in the CI pipeline, but for local executions we need this variable as well. See above local import in packages.yml.

#### Exasol dispatch utils

In order to use the exasol dispatch utils (also called [shim project](https://docs.getdbt.com/reference/project-configs/dispatch-config) in DBT) - we need to add the following entries in our project configuration. This needs to be added for all exasol projects that also use the dbt_utils package.

- [dbt_project.yml](https://docs.getdbt.com/reference/dbt_project.yml):
```yaml
dispatch:
  - macro_namespace: dbt_utils
    search_order: ['exasol_utils', 'dbt_utils']
```

- [packages.yml](https://docs.getdbt.com/docs/building-a-dbt-project/package-management/):
```yaml
packages:
  - package: dbt-labs/dbt_utils
    version: 0.8.0
  - git: "git@github.com:tglunde/exasol-utils.git"
```

#### Do not overuse Macros

[DBT Jinja macros](https://docs.getdbt.com/docs/building-a-dbt-project/jinja-macros/) usage for special cases when important business logic needs to be applied in several DBT models or across different applications, is ok, but should be kept to a minimal. The problem with Macros in Jinja is, that they are extremely hard to debug. If you put logic outside the SQL context, you cannot just send generated models to the database for testing anymore. Getting a python debugger to dive into Jinja macros is not an easy task. 

Therefore general guidelines we propose are:
- Think about KISS when using JINJA macros to implement business logic
- Macros should be short, less than a view lines long
- Try to first think of a good modularization by using view or ephemeral models in SQL only
- Do not use Macros to change default DBT behavior - like how schema names are build

### Model Naming
Models typically fit into following categories:
- staging models that source raw data 1:1
- rule models that contain transformation steps
- mart models which depict the final schema utilized by the frontend. By default these are usually still dimensional in different forms - therefore dim and fact models can be implemented

```
├ crm
├── dbt_project.yml
├── profiles.yml
├── packages.yml
└── models
    ├── dim
    |   └── crm_dim.yml
    |   └── crm_dim.docs
    |   └── dim_customers.sql
    |   └── fct_orders.sql
    ├── rules
    |   └── crm_rules.yml
    |   └── crm_rules.docs
    |   └── rul_customers_unioned.sql
    |   └── rul_customers_grouped.sql
    └── staging
        └── src_crm.yml
        └── src_crm.docs
        └── stg_crm.yml
        └── stg_crm_dwh_customers.sql
        └── stg_crm_dwh_invoices.sql
```
- All objects should be plural, such as: `stg_stripe_invoices`
- Staging tables are prefixed with `stg_`, such as: `stg_<source>_<schema>_<object>`
- Intermediate tables should end with a past tense verb indicating the action performed on the object, such as: `customers__unioned`
- Marts are categorized between fact (immutable, verbs) and dimensions (mutable, nouns) with a prefix that indicates either, such as: `fct_orders` or `dim_customers`
- each sub folder has a yml for schema documentation and tests as well as a docs file for additional documentation blocks used in overview pages and descriptions

### Model configuration

- Model-specific attributes (like sort/dist keys) should be specified in the model.
- If a particular configuration applies to all models in a directory, it should be specified in the `dbt_project.yml` file.
- In-model configurations should be specified like this:

```python
{{
  config(
    materialized = 'table',
    sort = 'id',
    dist = 'id'
  )
}}
```

### dbt conventions
* Only `stg_` models should select from `source`s.
* All other models should only select from other models.

### Testing

- Every subdirectory should contain a `.yml` file, in which each model in the subdirectory is tested. For staging folders, the naming structure should be `src_sourcename_schema_relation.yml`. For other folders, the structure should be `foldername.yml` (example `core.yml`).
- At a minimum, unique and not_null tests should be applied to the primary key of each model.

### Naming and field conventions

* Schema, table and column names should be in `snake_case`.
* Use names based on the _business_ terminology, rather than the source terminology.
* Each model should have a primary key.
* The primary key of a model should be named `<object>_id`, e.g. `account_id` – this makes it easier to know what `id` is being referenced in downstream joined models.
* For base/staging models, fields should be ordered in categories, where identifiers are first and timestamps are at the end.
* Timestamp columns should be named `<event>_at`, e.g. `created_at`, and should be in UTC. If a different timezone is being used, this should be indicated with a suffix, e.g. `created_at_pt`.
* Booleans should be prefixed with `is_` or `has_`.
* Price/revenue fields should be in decimal currency (e.g. `19.99` for $19.99; many app databases store prices as integers in cents). If non-decimal currency is used, indicate this with suffix, e.g. `price_in_cents`.
* Avoid reserved words as column names
* Consistency is key! Use the same field names across models where possible, e.g. a key to the `customers` table should be named `customer_id` rather than `user_id`.

### CTEs

- All `{{ ref('...') }}` statements should be placed in CTEs at the top of the file
- Where performance permits, CTEs should perform a single, logical unit of work.
- CTE names should be as verbose as needed to convey what they do
- CTEs with confusing or notable logic should be commented
- CTEs that are duplicated across models should be pulled out into their own models
- create a `final` or similar CTE that you select from as your last line of code. This makes it easier to debug code within a model (without having to comment out code!)
- CTEs should be formatted like this:
- CTEs should not be nested - it is not necessary and makes code harder to read and maintain

``` sql
with

events as (

    ...

),

-- CTE comments go here
filtered_events as (

    ...

)

select * from filtered_events
```

### SQL style guide

- Use trailing commas
- Indents should be four spaces (except for predicates, which should line up with the `where` keyword)
- Lines of SQL should be no longer than 80 characters
- Field names and function names should all be lowercase
- The `as` keyword should be used when aliasing a field
- Fields should be stated before aggregates / window functions
- Aggregations should be executed as early as possible before joining to another table.
- Ordering and grouping by a number (eg. group by 1, 2) is preferred over listing the column names. Note that if you are grouping by more than a few columns, it may be worth revisiting your model design.
- Specify join keys - do not use `using`. Certain warehouses have inconsistencies in `using` results (specifically Snowflake).
- Prefer `union all` to `union` 
- Avoid table aliases in join conditions (especially initialism) – it's harder to understand what the table called "c" is compared to "customers". Use CTEs instead where you can
- If joining two or more tables, _always_ prefix your column names with the table alias. If only selecting from one table, prefixes are not needed.
- Since we do not like outer it is also best to not differ between `inner join` and `join` since they are identical. Prefer left join over right join to keep the order the same. `left join` already is outer and instead of `full outer` consider SET operation with `union all` instead to merge the relations if required.

- *DO NOT OPTIMIZE FOR A SMALLER NUMBER OF LINES OF CODE. NEWLINES ARE CHEAP, BRAIN TIME IS EXPENSIVE*

- all SQL rules are best implemented with - [sqlfluff](https://www.sqlfluff.com/)

### Example SQL
```sql
with

my_data as (

    select * from {{ ref('my_data') }}

),

some_cte as (

    select * from {{ ref('some_cte') }}

),

some_cte_agg as (

    select
        id,
        sum(field_4) as total_field_4,
        max(field_5) as max_field_5

    from some_cte
    group by 1

),

final as (

    select [distinct]
        my_data.field_1,
        my_data.field_2,
        my_data.field_3,

        -- use line breaks to visually separate calculations into blocks
        case
            when my_data.cancellation_date is null
                and my_data.expiration_date is not null
                then expiration_date
            when my_data.cancellation_date is null
                then my_data.start_date + 7
            else my_data.cancellation_date
        end as cancellation_date,

        some_cte_agg.total_field_4,
        some_cte_agg.max_field_5

    from my_data
    left join some_cte_agg  
        on my_data.id = some_cte_agg.id
    where my_data.field_1 = 'abc'
        and (
            my_data.field_2 = 'def' or
            my_data.field_2 = 'ghi'
        )
    having count(*) > 1

)

select * from final

```

- Your join should list the "left" table first (i.e. the table you are selecting `from`):
```sql
select
    trips.*,
    drivers.rating as driver_rating,
    riders.rating as rider_rating

from trips
left join users as drivers
    on trips.driver_id = drivers.user_id
left join users as riders
    on trips.rider_id = riders.user_id

```

## YAML style guide

* Indents should be two spaces
* List items should be indented
* Use a new line to separate list items that are dictionaries where appropriate
* Lines of YAML should be no longer than 80 characters.

#### Example YAML
```yaml
version: 2

models:
  - name: events
    columns:
      - name: event_id
        description: This is a unique identifier for the event
        tests:
          - unique
          - not_null

      - name: event_time
        description: "When the event occurred in UTC (eg. 2018-01-01 12:00:00)"
        tests:
          - not_null

      - name: user_id
        description: The ID of the user who recorded the event
        tests:
          - not_null
          - relationships:
              to: ref('users')
              field: id
```


### Jinja style guide

* When using Jinja delimiters, use spaces on the inside of your delimiter, like `{{ this }}` instead of `{{this}}`
* Use newlines to visually indicate logical blocks of Jinja

## Further Reading
- [projects](https://discourse.getdbt.com/t/how-we-structure-our-dbt-projects/355)
- [cte](https://discourse.getdbt.com/t/why-the-fishtown-sql-style-guide-uses-so-many-ctes/1091)
- [group-by](https://blog.getdbt.com/write-better-sql-a-defense-of-group-by-1/)
- [union-all](http://docs.aws.amazon.com/redshift/latest/dg/c_example_unionall_query.html)
- [Understand Configs and Properties in DBT](https://docs.getdbt.com/reference/configs-and-properties)
- [Using Environment Variables in DBT](https://docs.getdbt.com/reference/dbt-jinja-functions/env_var)
- [DBT utils package](https://hub.getdbt.com/dbt-labs/dbt_utils/latest/)
- [DBT codegen package](https://hub.getdbt.com/dbt-labs/codegen/latest/)
- [Exasol DBT adapter](https://github.com/tglunde/dbt-exasol)
- [Exasol dbt-utiles shim package](https://github.com/tglunde/exasol-utils)
- [DBT Project Maturity](https://github.com/dbt-labs/dbt-project-maturity)
- [DBT Project Maturity](https://www.youtube.com/watch?v=jJFdVVzWCKI)
- [Modern Data Platform](https://towardsdatascience.com/building-an-end-to-end-open-source-modern-data-platform-c906be2f31bd)
