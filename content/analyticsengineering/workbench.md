---
title: "04 ACS Workbench"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Workbench
The workbench provides the user with all necessary tools to execute the data pipeline on disposable environments. 

Generally we differentiate between stage and environment:
- A stage is a kubernetes cluster on a dockerhost installed through k3d, using our stagecreate configuration
- (disposable) Environment: One isolated environment where a user can deploy all artifacts for the data pipeline in one kubernetes namespace

The creation of the different stages - which usually consist of development and staging for disposable environments that can run in a cluster.
{{< figure src="/wb_overview.png" alt="Workbench Overview">}}
In this overview sample, we can see that Development and Staging are being operated on one cluster. In this scenario, the namespaces need to be the differentiators for dev and staging environments - usually by prefixes like dev- and test- or staging-. The environment names have to correspond to Kubernetes namespace standard naming which derives from hostname - since network name resolution is also based on namespace names.

The workbench can be deployed as docker image or using a virtual machine installed on Debian with a standard desktop and the workbench pre-installed.

## Integrated Development Environment (IDE)
As IDE for the users, we recommend Visual Studio Code, as the integration of necessary tools is very good and it has good integration with Git:
- plugin for dbt available - navigation through DBT models and running commands on model parents and children
- sqlfluff for linting DBT models - providing feedback directly in the IDE on all DBT models
- Markdown and ASCIIdoc plugins to support documentation including spell checking
- YAML support for metadata, test and documentation editing
- DrawIO integration for illustrations

Currently there is no good integration of a database client to execute SQL directly from within VSCode. Therefor we recommend the usage of DBeaver, as it supports all databases DBT supports.

## Base CLI utilities
In accordance with DBT itself, we also provide command line utilities to handle all necessary tasks around the disposable environment and all steps through the data pipeline. We recommend using zsh terminal with ohmyzsh and dotenv extension for ease of use in the terminal.

### Prerequisites
#### ssh access to the dockerhost where the kubernetes cluster is running

SSH key access is recommended in order to control the disposable environments in the cluster. You need to generate a public/private key pair with ```ssh-keygen```. By default this generates the public key into /home/<<user>>/.ssh/id_rsa.pub. This file needs to be provided to the admin to grant access to the dockerhost via ssh.
Additionally in order to have the key cached by the sshagent process to cache e.g. the passphrase, the user might need to add the ssh-key (private) manually using ```ssh-add /path_to_key/id_rsa```.

#### Environment variables set
```
DOCKERHOST=172.105.85.155
DOCKERIP=$DOCKERHOST
STAGE=dev
KUSTOMIZE_ENV=dev
DOCKERUSER=root
DOCKERURL=$(echo "ssh://$DOCKERUSER@$DOCKERHOST")
DOCKER_HOST=$DOCKERURL
APIPORT=1666
HTTPSPORT=443
SSHPORT=2222
ADDDNS=$(echo "8.8.8.8 8.8.4.4")
DOMAINNAME=$(echo "$DOCKERIP.nip.io")
DBT_USER=sys
DBT_PASS=<password>
DBT_PROFILES_DIR=.
EXASOL_SERVICE_HOST=localhost
EXASOL_SERVICE_PORT=8563
```
The parameters for IP/hostname, ports and DOCKERUSER need to be provided by the admin that created the dockerhost and controls access to the technical user that gets access to the docker daemon via ssh.

3. okteto yaml for port forward and cluster access
Using okteto the user can have port forwards and local access in the cluster namespace for the disposable env. The following yaml needs to be filled in order to start okteto utility.
```
name: fifadwh
autocreate: true
image: alligatorcompany/acs-workbench:1.0.13
command: zsh
namespace: <DISPOSABLE ENVIRONMENT NAME>

workdir: /home/acs
sync:
  - .:/wks

environment:
  - STAGE=dev
  - DOMAINNAME=<DOMAINNAME>
  - DOCKER_HOST=ssh://<USER@HOST>
  - DBT_USER=sys
  - DBT_PASS=<password>
  - DBT_PROFILES_DIR=.
  - EXASOL_SERVICE_HOST=localhost
  - EXASOL_SERVICE_PORT=8563

resources:
  requests:
    memory: "128Mi"
    cpu: "500m"
  limits:
    memory: "128Mi"
    cpu: "500m"
forward:
  - 8563:exasol:8563
  - 8000:dvb:8000

securityContext:
  runAsUser: 1000
  runAsGroup: 1000
  fsGroup: 1000
```
Usually there is a template version of this YAML versioned in the root of the GIT repository, that leaves only the namespace / environment name to be changed in case you are switching between disposable environments. The other parameters - like the environment variables will be stable as long as you are working on the same stage / cluster.

### Disposable environments
The environment commands all write a log file into the script location ```which envinit```. Once the environment variables have been set and the ssh connection works ```ssh $DOCKER_HOST``` the commands are ready to be executed. You can also check the docker client connection by entering ```docker context ls```. The default context should reflect the DOCKER_HOST environment variable.
#### envinit
The ```envinit``` command is without parameter. It executes the k3d utility and acts according the the set environment variables. On successful run - return code 0 - your local kubectl config will be pointing to the cluster configured in your environment variables.
To check you can execute ```kubectl get nodes``` to see all nodes of the cluster:
```
k3d-dev-agent-1    Ready    <none>                 120d   v1.21.1+k3s1
k3d-dev-server-0   Ready    control-plane,master   120d   v1.21.1+k3s1
k3d-dev-agent-0    Ready    <none>                 120d   v1.21.1+k3s1
k3d-dev-agent-2    Ready    <none>                 120d   v1.21.1+k3s1
```
In this list you can also check, that you are connected to the right stage - here the stage is dev.

#### envcreate
The ```envcreate <ENVNAME>``` command creates a new disposable env as configured for your data pipeline. If you are using an on-prem database with Datavault Builder automation, it will create a new Kubernetes namespace with 
- Stateful database service e.g. Exasol instance
- Datavault Builder deployment with a pod containing all container images
- Secrets for the access and license
- Initial deployment for Datavault Builder

To check you can execute ```kubectl get all -n <<ENVNAME>>```. The sample output here would be:
```
NAME                                 READY   STATUS      RESTARTS   AGE
pod/acs-dvbdeploy-qtfxl-1795685057   0/2     Completed   0          21d
pod/acs-dvbdeploy-qtfxl-98027909     0/2     Completed   0          21d
pod/fifadwh-856dbfbc7c-rbzbx            1/1     Running     0          17d
pod/dvb-5474fbf498-rhd7v             5/5     Running     1          21d
pod/exasol-0                         1/1     Running     1          21d

NAME             TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/exasol   ClusterIP   10.43.5.44     <none>        8563/TCP   21d
service/dvb      ClusterIP   10.43.66.82    <none>        8000/TCP   21d
service/fifadwh     ClusterIP   10.43.183.88   <none>        8080/TCP   21d

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/fifadwh   1/1     1            1           21d
deployment.apps/dvb    1/1     1            1           21d

NAME                              DESIRED   CURRENT   READY   AGE
replicaset.apps/fifadwh-6dc99bfbd6   0         0         0       21d
replicaset.apps/fifadwh-856dbfbc7c   1         1         1       17d
replicaset.apps/dvb-5474fbf498    1         1         1       21d

NAME                      READY   AGE
statefulset.apps/exasol   1/1     21d
```
All STATUS should be completed or running. Another way to dig deeper into your disposable env is k9s.

In case of cloud warehouses the Statefulset for the on-prem will be missing. In case you are using DBTvault instead of Datavault Builder for the automation, then there will be no additional Deployment for that either.

After successful start of you environment, you can start local development by executing ```okteto up``` - usually in the root project directory, where okteto.yml.template is versioned.
```
➜ fifa git:(feature/documentation) ✗ okteto up 
 ✓  Images successfully pulled
 ✓  Files synchronized
    Context:   k3d-dev
    Namespace: dev-torsten
    Name:      fifadwh
    Forward:   8001 -> dvb:8000
               8564 -> exasol:8563

```
On your local machine you can now access
- Datavault Builder at http(s)://localhost:8001
- Exasol at localhost on port 8564

Using the above sample environment, also DBT should now be configured properly using this profiles.yml:
```
dbt:
  target: dev
  outputs:
    dev:
      type: exasol
      threads: 1
      dsn: "{{ env_var('EXASOL_SERVICE_HOST') }}:{{ env_var('EXASOL_SERVICE_PORT') }}"
      user: "{{ env_var('DBT_USER') }}"
      pass: "{{ env_var('DBT_PASS') }}"
      dbname: dbt
      schema: ZZZ_Q3_SRC

config:
  send_anonymous_usage_stats: False
```

#### envdelete
The ```envdelete <<ENVNAME>>``` deletes the namespace and all persistent data that might be living in that context.
#### envlist
The ```envlist``` command lists all namespaces of the cluster - to check on existing disposable environments.
### Code Generation
The ``` dwhgen ``` utility can, based on the DBT hardrule projects generate artifacts for you:
- source.yml - relation (table or view) structures with fields and comments if supported by the sqlalchemy driver
- stg_*.sql - DBT staging models with Schema on Read (SoR) views
- seed_*.yml - Column datatype definitions for source seeds in order to help dbt seed to create proper datatypes - also for empty columns

In order to make this generation process work, you need to define the following additional metadata tags in your source.yml DBT definition for every source and interface:
```
version: 2

sources:
  - name: Fifa
    description: Fifa football gaming association open data
    schema: YYY_Q3_PSA
    meta:
      dburl: '<URL in SQLALCHEMY format>'
    tables:
      - name: <SOURCE_SCHEMA_INTERFACE>
        meta:
          source_schema: 'schema'
          source_name: 'name'
          unique_index: 'field1,field2,field3'

```
- dburl in sqlalchemy format: <<DB-driver>>://<<host>>:<<port>>/<<DBNAME>>
- source_schema: Database or interface schema name
- unique_index: Comma separated field list of technical key for PSA integration

After executing ```dwhgen```, you will find the generated artifacts in the target directory. The integration of the data into your GIT repository needs to be done manually.

Parameters:
- target (-t): directory for generated artifacts
- database (-d): name of target warehouse (Exasol, Snowflake, )
- project (-p): root directory of your GIT repository containing 'hardrule' tagged dbt projects
- sources (-s): comma separated list of sources to include - takes all if omitted

Here is another sample for the hardrule tag in the dbt_project.yml  - for every hardrule project in the GIT repository:
```
name: 'fifa_hr'
version: '1.0.0'
config-version: 2
profile: 'dbt'

models:
  +post-hook: 'grant select on {{this}} to public'
  +tags:
    - 'hardrule' OR 'softrule'
```
### k9s - CLI Kubernetes Dashboard
The command ```k9s``` is a very helpful CLI tool to investigate Kubernetes resources without typing over-lengthy kubectl commands. Main control keys are
- cursor keys for navigation
- `:´  to enter resource you want to list
- ESC to go back
- CTRL-c for or ': q RETURN' for exit
- all other keys are mentioned on top of the screen

{{< figure src="/k9s.png" alt="k9s">}}

### Tooling

For installation the ACS workbench (acswb) is published as docker image. the acswb is a shell script wrapping the docker run options needed to setup the environment. 

Prerequisites:
- Internet connection
- Linux operating system or WLS on Windows
- docker-ce daemon (linux) running

Installation:
- Downloading the script from the gitlab repository, 
- Making it executable `chmod +x acswb` 
- Putting it on the target machine's PATH

The following internal command line interfaces can be called by pre-pending the acswb script `acsweb kubectl`:
- kubectl
- k3d
- k9s
- argo
- docker-cli
- kustomize
- ssh-add 
- ssh-agent

# Further Reading
- [VSCode ssh remote how-to & Troubleshooting](https://code.visualstudio.com/docs/remote/ssh)
- [k9s presentation](https://www.youtube.com/watch?v=boaW9odvRCc)
- [Kubernetes Crash course](https://www.youtube.com/watch?v=s_o8dwzRlu4)
- [Shell setup OhMyZsh](https://www.bretfisher.com/shell/)
