---
title: "HowTo Setpu your environment"
date: 2021-11-23T03:01:15+01:00
draft: true
---

# Setup Disposable Environment

## Local installation
- useful zsh plugins:
```
...
plugins=(git docker docker-compose kubectl pyenv pod sudo vscode dotenv)
...
```

- docker: https://download.docker.com/
- k3d https://k3d.io/v5.2.1/#installation
- kubectl: https://kubernetes.io/de/docs/tasks/tools/install-kubectl/
- k9s: https://github.com/derailed/k9s/releases
- kustomize: https://kubectl.docs.kubernetes.io/installation/kustomize/
- argo: https://github.com/argoproj/argo-workflows/releases/tag/v3.1.15
- okteto: https://okteto.com/docs/getting-started/installation/
- git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- python: 3.9.x - 64-bit: https://www.python.org/downloads/release/python-399/
- dbt: pip install dbt==0.21.1
- sqlfluff: pip install sqlfluff==0.8.1
- dbt-exasol: pip install dbt-exasol==1.0.2
- vscode: https://code.visualstudio.com/Download
- ssh-keygen + ssh-add + ssh-agent
- acs-operation

## Setup local environment

- environment .env
```
STAGE=dev
DOMAINNAME=dev.172.105.260.48
DOCKERIP=172.105.260.48
DOCKERURL=ssh://dwh@172.105.250.48
DOCKER_HOST=ssh://dwh@172.105.250.48
DBT_PROFILES_DIR=.
DBT_USER=sys
DBT_PASS='start123'
EXASOL_SERVICE_HOST=localhost
EXASOL_SERVICE_PORT=8564
```
source .env file

- docker context ls
- envinit - connecting kubectl ~/.kube/config to the correct environment

zsh extension dotenv

envinit
## Create Environment

envlist

envcreate <name>

setup okteto.yml

connect exasol

connect dvb

## Deploy models

### DBT

### DVB
