---
title: "HowTo Dataset A to Z"
date: 2021-11-23T03:01:15+01:00
draft: false
---

# Dataset journey from A to Z

## Starting the User Story

First we are creating the feature branch, that will represent the changes needed for our User Story. Also, since it will be needed in the end anyhow we can already start documenting our future merge request by creating it based on our new branch.

```bash
git checkout -b feature/<<ticketnumber>>-add-additional-league-source
git push -set-upstream-branch 
```
{{< figure src="/hta2z_gitlabcreatebranch.png" alt="Gitlab create new feature branch" alt="Feature Branch">}}
{{< figure src="/hta2z_gitlabcreatemr.png" alt="Gitlab create new feature branch" alt="Merge Request">}}

In case you create the branch on your remote git provider, on your local repository you need to execute the following to retrieve the ref to that branch:
```bash
git pull --all
```

Now is a good time to review the User Story requirements and think about what we need to do. This understanding can be documented in the merge request as documentation for the reviewer as to what to pay attention to. Using this developer description, the product owner and the developer have a chance to re-sync and get to a common understanding of the requirements. 

Clarify expectations:

- Is the requirement story DoR - with logical data flow, ELM backbone model and CBC forms source technical, business key - has there been an identifier set by the ELM workshop?
- Are there datasets to have acceptance tests run against?

## Create dbt project skeletons for Hardrule and Softrule
In our story, we need to add the Hardrule and Softrule project, so we follow the following steps:
1. Create Hardrule project: `mkdir -p 00_src/fifa_hr/models`
2. Create Softrule project: `mkdir -p 02_app/fifa/models`
3. Create minimal project yml - replace the name and schema - which is the suffix for the Hardrule/Softrule schema
```yml
name: 'fifa'
version: '1.0.0'
config-version: 2

profile: 'dbt'

models:
  +post-hook: 'grant select on {{this}} to public'
  +tags:
    - 'hardrule'
  schema: fifa

dispatch:
  - macro_namespace: dbt_utils
    search_order: ['exasol_utils', 'dbt_utils']
```
4. Create profiles.yml 
```yml
dbt:
  target: dev
  outputs:
    dev:
      type: exasol
      threads: 1
      dsn: "{{ env_var('EXASOL_SERVICE_HOST') }}:{{ env_var('EXASOL_SERVICE_PORT') }}"
      user: "{{ env_var('DBT_USER') }}"
      pass: "{{ env_var('DBT_PASS') }}"
      dbname: dbt
      schema: ZZZ_Q3_SRC

    ci:
      type: exasol
      threads: 1
      dsn: "{{ env_var('EXASOL_SERVICE_HOST') }}:{{ env_var('EXASOL_SERVICE_PORT') }}"
      user: "{{ env_var('DBT_USER') }}"
      pass: "{{ env_var('DBT_PASS') }}"
      dbname: dbt
      schema: ZZZ_Q3_SRC

config:
  send_anonymous_usage_stats: False
```
- replace schema for Hardrule with ZZZ_Q3_SRC - and Softrule with AAA_Q2_APP
- for Hardrule it is necessary to duplicate the target for ci - in order to switch between source seeds and psa SOR
- for Softrule you can omit the ci target

5. Create the following default packages.yml
```yml
packages:
  - package: dbt-labs/codegen
    version: 0.4.1
  - package: dbt-labs/dbt_utils
    version: 0.7.6
  - git: "git@github.com:tglunde/exasol-utils.git"
```
- Execute the following to test correctness
```bash
dbt deps && dbt debug
```
6. Adding Hardrule and Softrule project to Makefile to simplify local testing
In `00_src` and `02_app` are corresponding Makefiles with the corresponding dbt command executions for all sub projects:
```bash
do-everything:
	PRJ=./00_src/psa && dbt deps --project-dir $$PRJ \
		&& dbt seed --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./00_src/fifa_hr && dbt deps --project-dir $$PRJ && \
		dbt run --target ci --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt test --target ci --project-dir $$PRJ --profiles-dir $$PRJ

	PRJ=./02_app/fifa && dbt deps --project-dir $$PRJ && \
		dbt seed --project-dir $$PRJ --profiles-dir $$PRJ && \
		dbt run --project-dir $$PRJ --profiles-dir $$PRJ
		dbt test --project-dir $$PRJ --profiles-dir $$PRJ
```
- Hardrule project is executed with `--target ci` in order to load the seeds from the PSA
- Softrule project also seeds for acceptance test data

### Add Jobs to the Argo Workflows

Adding the Hardrule project into the dag - no dependencies needed:
```yml
      - name: fifa
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/00_src/fifa_hr/'}]
```

Adding the Softrule project - depending on the datavault - or other rules projects, that are imported into the packages.yml:
```yml
      - name: fifa
        dependencies: [ datavault ]
        templateRef:
          name: wf-elt-run
          template: wf-elt-run
        arguments:
          parameters: [{name: command, value: 'run'}, {name: projectdir, value: '/code/02_app/fifa/'}]
```

## Test Driven - Seeding test data first

### Hardrule - Source expectations

Seeding source test data in order to fill the pipeline with some data to chew on. Remeber - selection is not really critical - but should be small to load fast. The csv file needs to be placed under `00_src/psa/data/<<sourcename>>` and source definitions should be added to the corresponding source.yml file.

In case you have acceptance test data - remember to have the source data criteria matching those acceptance dataset. Also filtering our not needed datasets, because source seeds can be re-used by many acceptance level tests.

Good news is, that some of the Hardrule related artifacts can be generated using dbt-dwhgen. Since generation is based on relational dbms - it is also a good idea to first create the seed data and make sure these columns have correct datatypes. In case of non-relational sources you can use the CSV seeded data to have dbt-dwhgen work on those tables.

In order to start the dbt-dwhgen, you need a stub of the source.yml in your hardrule project, in order to have the tool find the relations you want to be used for the generation:
```/models/source.yml
  - name: fifa
    description: ''
    schema: yyy_q3_psa
    meta:
      dburl: 'db2+ibm_db://192.168.211.56:50000/testdb'
    tables:
      - name: player
        meta:
          source_schema: 'db2inst1'
          unique_index: 'id'
      - name: league
        meta:
          source_schema: 'db2inst1'
          unique_index: 'id'
      - name: match
        meta:
          source_schema: 'db2inst1'
          unique_index: 'id'
```
This would generate dbt artifacts for the tables player, league, match - that can be found at the above defined dburl - using the unique_index as technical keys for the psa integration and the SOR views. This is the corresponding dbt-dwhgen call:
```bash
acswb dwhgen -s fifa -d exasol -p .
```

Currently keywords

This will create a target folder in the current directory containing
- one sql file per source table with the stg dbt model including the ci and sor parts
- source.yml - seed definition for corresponding seed datatypes
```source.yml
version: 2

sources:
  - name: fifa
    description: 'n/a'
    schema: yyy_q3_psa
    meta:
      dburl: 'db2+ibm_db://192.168.211.56:50000/testdb'
    tables:
      - name: player
        meta:
          source_schema: 'prod'
          source_name: 'db2inst1'
          unique_index: 'id'
        description: ''
        columns:
        - name: id
          description: 'None'
          tests:
            - unique
            - not_null
        - name: p_projektnr
          description: 'None'

...
```
- seeds.yml - source table definitions including columns and descriptions - if supported by the sqlalchemy driver for your source dbms
```seeds.yml
seeds:
  +quote_columns: true
  PSA:
    league:
      +column_types:
        id: bigint
...
```

After generation, you need to merge the target files into the Hardrule project manually.

As you can see in the generated source.yml the schema test for the technical key has already been added to be unique and not null. If expectations for source values have been defined, you can add additional schema tests to this source definition.

### Concluding the Hardrule

After having the Hardrule project, staging views and schema tests in place, we can add corresponding `./00_src/fifa_hr/models/hr` models. In the simplest way we only need to select each source 1:1. Datatype conversions are also included in the staging views.
```sql
select
    {{ dbt_utils.star(ref('player')) }}
from {{ ref('player') }}
```

This generates a field list according to the staging model that got generated perviously.

Another example is the union - in case of different locations - or in the fifa example languages in order to UNION these source sets:
```sql
{{ dbt_utils.union_relations(
    relations=[ref('stg_league_dbo_league_de'), 
        ref('stg_league_dbo_league_en')],
    source_column_name='source_language'
)}}
```

Now we have two hardrule models, that can be loaded into our datavault - from the seeds or the psa depending on the `--target ci` parameter.

### Softrule - Application requirements and expectations

For acceptance tests on the Q2 application level, you create the seeds as usual in `./02_app/fifa/data/` folder - optionally adding a seeds.yml to set proper datatypes. If imported to the exasol once you can also use this as base for the dbt-dwhgen generation as described above.

On those acceptance tests, one easy way to test this dataset is to add data test in `/02_app/fifa/test` folder as dbt models. To follow the test driven approach you could now already create empty target models in `/02_app/fifa/models/dim` - here we assume a dim_player and a fct_match model and seeds of acc_player and acc_match. Then tests could be added for record count and set comparison like this:
- count test
```sql
with

count_diff as (
    select
        (select count(*) from {{ ref('dim_player') }} where id is not null)
        - (select count(*) from {{ ref('acc_player') }})
        as row_no

)

select row_no from count_diff

where row_no != 0
```
- compare complete set
```sql
(
    select * from {{ ref('dim_player') }}
    except
    select * from {{ ref('acc_player') }}
)

union all

(
    select * from {{ ref('acc_player') }}
    except
    select * from {{ ref('dim_player') }}
)

```

Now we can implement the User Story until those tests turn green.

## Incrementing the Datavault Model

When implementing the Datavault part of our UserStory, first thought should be to prepare our disposable env for easy handling:
- empty my disposable env by deploying against empty model
- which parts of the versioned Datavault model `01_dv/dvb` do I really need to deploy
- do I need the master version I just branched off of - or is the sprint start version enough?
- Is my source and target application comletely new? Do I need existing parts of the model at all?

Now, only deploy those parts that are necessary - either from the versioned `01_dv/dvb` folder or from the last sprint deployed disposable env.

We will assume everything is new and therefor we do not import anything, hence working against a completely empty disposable env - which is not only the fastest option, but also makes merging the metadata back into git easier, limiting the change set.

The steps to source and output data in the Datavault are:

1. Implement the new elements in your Datavault model (only change if you are ready to include data migrations) - by default we never change any Datavault elements. 
- Adding Hub Player
- Adding Hub League
- Adding Link between Player and League as 1:n

2. Add the source from the hardrule view in ZZZ_Q3_SRC_FIFA - in our case this is hr_player and hr_league staging tables added to a newly created source called 'fifa'
3. Adding the hub loads for Player and League - creating default satellites with all attributes - since there is no loads existing
4. Add hub foreign key load do Player from League (the reference in League that goes to Player) - to make sure there is no missed hub keys from the foreign key site of the relation.
5. Add link load
6. Finally, we can create two dimensional views for the businessobject schema to be imported into our Q2 `02_app/fifa` project to build our rules and dimensional model
- By default we include the corresponding Hub as source grain - because we assume we want all data from all sources
- Hashkey and Business Key should be renamed to HK_<<fieldname>> and BK_<<fieldname>> for easier usage. 
- The League view should also contain the HK_PLAYER from the hub that is connected through the link in order to be able to build a virtualized star schema, that reuses those HK - for optimal join performance in the Datavault

## Create The Dimensional Model

### Prepare the input from the Datavault
In our dimensional model dbt project `02_app/fifa`, that we already created in our test driven approach with some tests, we first need to import the entity views, that provide us with our integrated and business target model entities.

In our case these are - according to Datavault Builder standards in the businessobjects schema:
- PLAYER_S_DVB_HUB - the player data
- LEAGUE_S_DVB_HUB - the match data from different leagues
These will be imported by source definitions into our DBT project using `02_app/fifa/models/source.yml:
```yml
version: 2

sources:
  - name: datavault
    description: ''
    schema: businessobjects
    tables:
      - name: league_s_dvb_hub
      - name: player_s_dvb_hub
```

Correspondingly, we integrate simple staging models like 

```sql
select
    {{ dbt_utils.star(source('datavault','league_s_dvb_hub')) }} 
from {{ source('datavault','league_s_dvb_hub') }}
```

### Create a simple star schema
Re-using the granularity of the league as fact grain in order to count the matches a player did:

```sql
with

league as (
    select * from ref('stg_league')
)

player as (
    select * from ref('player')
)

select
    league.hk_league,
    league.hk_player,
    1 as match_count

from league join player on league.hk_player = player.hk_player
```

And the simple dimension for player:
```sql
with

league as (
    select * from ref('stg_league')
)

player as (
    select * from ref('stg_player')
)

select
    player.hk_player,
    player.name

from player 

where exists (select 1 from league where league.hk_player = player.hk_player)
```

### Adding Schema Tests and Documentation

For existing models, that have already be run in our database, we can use the dbt-labs utility codegen in order to generate the documentation for the model:
```bash
dbt run-operation generate_model_yaml --args '{"model_name": "dim_player"}'
```

The model gets generated into the terminal - that can be copied to the `02_app/fifa/models/schema.yml`. Here we can add the column based schema tests for uniqueness and relationship tests for our star schema.
```yml
version: 2

models:
  - name: dim_player
    description: '{{ doc("dim_player") }}'
    columns:
      - name: hk_player
        description: "Player surrogate key"
        tests:
        - not_null
        - unique
      - name: name
        description: "Player name"
        tests:
        - not_null

  - name: fct_league
    description: '{{ doc("fct_league") }}'
    tests:
      - unique:
          column_name: "(hk_league || '-' || hk_player )"
    columns:
      - name: hk_player
        description: "Foreign Key reference to player dimension"
        tests:
        - not_null
        - relationships:
            to: ref('dim_player')
            field: hk_player
      - name: match_count
        description: "1 if player took part, 0 else"
        tests:
        - not_null
        - dbt_utils.accepted_range:
            min_value: 0
            inclusive: true
```

Additionally we need to add the overview and table descriptions in so called "docblocks" in the `02_app/fifa/models/docs.md` file:

```Markdown
{% docs __overview__ %}
# Fifa Football Analysis

## Report Overview

## Data Requirements

### Enterprise Data Story

### ELM - Ensemble Logical Model

#### CBC list

### Dimensional Model

## Functional Dataflow

### Interfaces

### Golden Source Rules

### Hardrules

### Softrules
{% enddocs %}

{% docs fct_league %}
### League facts
{% enddocs %}
{% docs dim_player %}
### Player dimension
{% enddocs %}
```