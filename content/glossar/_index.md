---
title: "99 Glossar"
date: 2022-04-01T17:00:00+02:00
draft: true
---

## Glossar Index

### business process model (BPM/N)

### data architecture 

### data layer

###  data point model

Conceptual model.
Describes structure of information elements as combination of multiple dimensions (defining and identifying a data point) with metrics/KPIs.
Defines rule for navigation, filtering and calculation in regard to dimensions and metrics.

### data science models

Data Science: math. Modelle, ML, AI/KI, ...

### data vault model

### DataOps 

DataOps := automation & observability & multi-model & multi-purpose

### decision support

### ensemble logical model

### FCO-IM - fact modeling

### graph data model

### information element

Kleinstes(?) Element des Informationsmodells.

Informationselement := Datenelement, Feature, KPI/Metrik   (im Sinne eines Datenpunkt, muß mit mehr als einer Zahl beschrieben werden).

### information model

DAS "Information Modell" (Conceptual!!)  
im Sinne von "Daten + Semantik/fachl. Definition"

### JSON schema

document - JSON-schema

### logical data model

### metrics / kpi

### multi-dimensional implementation

- deformed Kimball
- starschema
- OLAP engines

-> see: semantic layer

### 'physical' model

### property graph

Property Graph - Vernetzung von Informationen (Informationselementen) mit Personen, Prozessen, Konzepten, ...

### starschema

unbalanced implementation approach








