FROM peaceiris/hugo:v0.88.1-full as build
RUN git submodule update --init && hugo

FROM nginx:alpine
COPY --from=build /public /usr/share/nginx/html
WORKDIR /usr/share/nginx/html
