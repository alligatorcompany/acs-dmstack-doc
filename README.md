# Documentation for ACS Stack implementation

This is the documentation for the Alligator Company Software modern data platform stack. It is based on Analytics Engineering with the Data Build Tool, Datavault as central integration point and DataOps principles for fast and quality delivery.

The documentation is build with https://gohugo.io/ static site generator and the book theme.

To start the server, after installing the hugo client, please execute the following commands in a terminal window to look at the documenation in your browser:
```
git clone ...
git submodule update --init --recursive --remote --merge
hugo serve
open http://localhost:1313
```
please make sure to install the extended version

We follow the standard directory layout for hugo websites.

We publish this work under the CC license https://creativecommons.org/licenses/by-sa/4.0/legalcode - Share-Alike. Your are allowed to use and modify this work, if you keep a reference to our original source.

# Principles
We have build a package that contains different parts of a data architecture, that obviously can be used in probably a thousand different ways. The way we envision this to look like expresses our longterm experiences with data warehouse and cloud warehouse projects in Europe.

This is our opinion and we believe in this rules and prescribed ways of approaching analytics, Datavault and DataOps to play well together and make sense in combination. This opinion may change, if we face new realities and good arguments, but until then, we see it as our obilgation to keep these rules together for the good of the whole.

# Contribution

Contributions are very welcome. Please fork this repository and create merge requests with proposed changes. Please refer to the Contributing guide.
